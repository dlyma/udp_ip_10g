/************************************************
  Copyright (c) 2016, Xilinx, Inc.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, 
  this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, 
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors 
  may be used to endorse or promote products derived from this software 
  without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.// Copyright (c) 2015 Xilinx, Inc.
 ************************************************/
#include "udpAppMux.hpp"

using namespace std;

int main() {
    stream<axiWord>         rxDataIn("rxDataIn");
    stream<metadata>        rxMetadataIn("rxMetadataIn");
    stream<axiWord>         rxDataOutDhcp("rxDataOutDhcp");
    stream<metadata>        rxMetadataOutDhcp("rxMetadataOutDhcp");
    stream<axiWord>         rxDataOutApp_0;
    stream<metadata>        rxMetadataOutApp_0;
    ap_uint<16>             rxPortApp0 = 0x281;
    stream<axiWord>         rxDataOutApp_1;
    stream<metadata>        rxMetadataOutApp_1;
    ap_uint<16>             rxPortApp1 = 0x282;

    stream<ap_uint<16> >    requestPortOpenOut("requestPortOpenOut");
    stream<bool >           portOpenReplyIn("portOpenReplyIn");
    stream<ap_uint<16> >    requestPortOpenInDhcp("requestPortOpenInDhcp");
    stream<bool >           portOpenReplyOutDhcp("portOpenReplyOutDhcp");
    stream<ap_uint<16> >    requestPortOpenInApp_0;
    stream<bool >           portOpenReplyOutApp_0;
    stream<ap_uint<16> >    requestPortOpenInApp_1;
    stream<bool >           portOpenReplyOutApp_1;

    stream<axiWord>         txDataInDhcp("txDataInDhcp");
    stream<metadata>        txMetadataInDhcp("txMetadataInDhcp");
    stream<ap_uint<16> >    txLengthInDhcp("txLengthInDhcp");
    stream<axiWord>         txDataInApp_0;
    stream<metadata>        txMetadataInApp_0;
    stream<ap_uint<16> >    txLengthInApp_0;
    stream<axiWord>         txDataInApp_1;
    stream<metadata>        txMetadataInApp_1;
    stream<ap_uint<16> >    txLengthInApp_1;
    stream<axiWord>         txDataOut("txDataOut");
    stream<metadata>        txMetadataOut("txMetadataOut");
    stream<ap_uint<16> >    txLengthOut("txLengthOut");

    stream<axiWord> bufferQueue("bufferQueue");
    uint32_t    packetLength = 0;
    axiWord inData;

    ifstream inputFile;
    ifstream outputFile;

    static ap_uint<32> ipAddress = 0x01010101;

    inputFile.open("../../../../in_rx.dat");
    if (!inputFile) {
        cout << "Error: could not open test input file." << endl;
        return -1;
    }


    uint32_t count = 0;
    uint16_t keepTemp;
    uint64_t dataTemp;
    uint16_t lastTemp;

    //RX PORT tests
    uint32_t srcAddrTemp;
    uint16_t srcPortTemp;

    uint32_t destAddrTemp;
    uint16_t destPortTemp;

    metadata metadataInTemp;
    axiWord  dataInTemp;
    while(!inputFile.eof())
    {
        inputFile >> std::hex >> srcPortTemp >> srcAddrTemp >> destPortTemp >> destAddrTemp;
        //cout << hex << noshowbase << setfill('0');
        //cout << setw(4) << srcPortTemp;
        //cout << setw(8) << srcAddrTemp;
        //cout << setw(4) << destPortTemp;
        //cout << setw(8) << destAddrTemp;
        //cout << endl;
        metadataInTemp.sourceSocket.port = srcPortTemp;
        metadataInTemp.sourceSocket.addr = srcAddrTemp;

        metadataInTemp.destinationSocket.port = destPortTemp;
        metadataInTemp.destinationSocket.addr = destAddrTemp;
        rxMetadataIn.write(metadataInTemp);
        do {
            inputFile >> std::hex >> dataTemp >> keepTemp >> lastTemp;
            //cout << setw(16) << dataTemp;
            //cout << setw(4) << keepTemp;
            //cout << setw(1) << lastTemp;
            //cout << endl;
            dataInTemp.data = dataTemp;
            dataInTemp.keep = keepTemp;
            dataInTemp.last = lastTemp;
            rxDataIn.write(dataInTemp); 
        }  while(lastTemp == 0);
        //metadata rxMetadataInTest[] = 
        //{
        //{{0x100, 0x01010101}, {0x044, 0x010101010}},
        //{{0x100, 0x01010101}, {0x0281, 0x010101010}},
        //{{0x100, 0x01010101}, {0x0282, 0x010101010}}
        //};
    }

    int num_data;

    outputFile.open("../../../../out_golden_port0.dat");
    if (!outputFile) {
        cout << "Error: could not open test output file." << endl;
        return -1;
    }

    cout << "Testing port 0" << endl;
    outputFile >> std::dec >> num_data;
    while(num_data)
    {
        //read metadata
        outputFile >> std::hex >> srcPortTemp >> srcAddrTemp >> destPortTemp >> destAddrTemp;

        while(rxMetadataOutDhcp.empty())
        {
            udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                    rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                    rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                    requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                    requestPortOpenInApp_0,portOpenReplyOutApp_0,
                    requestPortOpenInApp_1,portOpenReplyOutApp_0,
                    txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                    txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                    txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                    txDataOut, txMetadataOut, txLengthOut);
        }

        metadata metadataInTemp = rxMetadataOutDhcp.read();
        if(metadataInTemp.sourceSocket.addr != srcAddrTemp ||
                metadataInTemp.sourceSocket.port != srcPortTemp ||
                metadataInTemp.destinationSocket.addr != destAddrTemp ||
                metadataInTemp.destinationSocket.port != destPortTemp)
        {
            cout << num_data << endl;
            cout << "Metadata error!" << endl;
            cout << "Expected: " << endl;
            cout << "{{" << srcPortTemp << "," << srcAddrTemp << "},{" << destPortTemp << "," << destAddrTemp << "}}" << endl; 
            cout << "{{" << metadataInTemp.sourceSocket.port << "," << metadataInTemp.sourceSocket.addr << "},{" << metadataInTemp.destinationSocket.port << "," << metadataInTemp.destinationSocket.addr << "}}" << endl; 
            return -1;
        }

        do
        {
            //read data
            outputFile >> std::hex >> dataTemp >> keepTemp >> lastTemp;
            while(rxDataOutDhcp.empty())
            {
                udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                        rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                        rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                        requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                        requestPortOpenInApp_0,portOpenReplyOutApp_0,
                        requestPortOpenInApp_1,portOpenReplyOutApp_0,
                        txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                        txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                        txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                        txDataOut, txMetadataOut, txLengthOut);
            }

            dataInTemp = rxDataOutDhcp.read();
            if(dataTemp != dataInTemp.data ||
                    keepTemp != dataInTemp.keep ||
                    lastTemp != dataInTemp.last)
            {
                cout << num_data << endl;
                cout << "Data error!" << endl;
                cout << "Expected: " << endl;
                cout << "{" << dataTemp << "," << keepTemp << "," << lastTemp << "}" << endl; 
                cout << "{" << dataInTemp.data << "," << dataInTemp.keep << "," << dataInTemp.last << "}" << endl; 
                return -1;
            }
        }  while(!dataInTemp.last);
        --num_data;

    }
    outputFile.close();

    outputFile.open("../../../../out_golden_port1.dat");
    if (!outputFile) {
        cout << "Error: could not open test output file for port1." << endl;
        return -1;
    }

    cout << "Testing port 1" << endl;
    outputFile >> std::dec >> num_data;
    while(num_data)
    {
        //read metadata
        outputFile >> std::hex >> srcPortTemp >> srcAddrTemp >> destPortTemp >> destAddrTemp;

        while(rxMetadataOutApp_0.empty())
        {
            udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                    rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                    rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                    requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                    requestPortOpenInApp_0,portOpenReplyOutApp_0,
                    requestPortOpenInApp_1,portOpenReplyOutApp_0,
                    txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                    txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                    txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                    txDataOut, txMetadataOut, txLengthOut);
        }

        metadata metadataInTemp = rxMetadataOutApp_0.read();
        if(metadataInTemp.sourceSocket.addr != srcAddrTemp ||
                metadataInTemp.sourceSocket.port != srcPortTemp ||
                metadataInTemp.destinationSocket.addr != destAddrTemp ||
                metadataInTemp.destinationSocket.port != destPortTemp)
        {
            cout << num_data << endl;
            cout << "Metadata error!" << endl;
            cout << "Expected: " << endl;
            cout << "{{" << srcPortTemp << "," << srcAddrTemp << "},{" << destPortTemp << "," << destAddrTemp << "}}" << endl; 
            cout << "{{" << metadataInTemp.sourceSocket.port << "," << metadataInTemp.sourceSocket.addr << "},{" << metadataInTemp.destinationSocket.port << "," << metadataInTemp.destinationSocket.addr << "}}" << endl; 
            return -1;
        }

        do
        {
            //read data
            outputFile >> std::hex >> dataTemp >> keepTemp >> lastTemp;
            while(rxDataOutApp_0.empty())
            {
                udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                        rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                        rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                        requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                        requestPortOpenInApp_0,portOpenReplyOutApp_0,
                        requestPortOpenInApp_1,portOpenReplyOutApp_0,
                        txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                        txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                        txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                        txDataOut, txMetadataOut, txLengthOut);
            }

            dataInTemp = rxDataOutApp_0.read();
            if(dataTemp != dataInTemp.data ||
                    keepTemp != dataInTemp.keep ||
                    lastTemp != dataInTemp.last)
            {
                cout << num_data << endl;
                cout << "Data error!" << endl;
                cout << "Expected: " << endl;
                cout << "{" << dataTemp << "," << keepTemp << "," << lastTemp << "}" << endl; 
                cout << "{" << dataInTemp.data << "," << dataInTemp.keep << "," << dataInTemp.last << "}" << endl; 
                return -1;
            }
        }  while(!dataInTemp.last);
        --num_data;

    }
    outputFile.close();

    outputFile.open("../../../../out_golden_port2.dat");
    if (!outputFile) {
        cout << "Error: could not open test output file for port2." << endl;
        return -1;
    }
    cout << "Testing port 2" << endl;
    outputFile >> std::dec >> num_data;
    while(num_data)
    {
        //read metadata
        outputFile >> std::hex >> srcPortTemp >> srcAddrTemp >> destPortTemp >> destAddrTemp;

        while(rxMetadataOutApp_1.empty())
        {
            udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                    rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                    rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                    requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                    requestPortOpenInApp_0,portOpenReplyOutApp_0,
                    requestPortOpenInApp_1,portOpenReplyOutApp_0,
                    txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                    txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                    txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                    txDataOut, txMetadataOut, txLengthOut);
        }

        metadata metadataInTemp = rxMetadataOutApp_1.read();
        if(metadataInTemp.sourceSocket.addr != srcAddrTemp ||
                metadataInTemp.sourceSocket.port != srcPortTemp ||
                metadataInTemp.destinationSocket.addr != destAddrTemp ||
                metadataInTemp.destinationSocket.port != destPortTemp)
        {
            cout << num_data << endl;
            cout << "Metadata error!" << endl;
            cout << "Expected: " << endl;
            cout << "{{" << srcPortTemp << "," << srcAddrTemp << "},{" << destPortTemp << "," << destAddrTemp << "}}" << endl; 
            cout << "{{" << metadataInTemp.sourceSocket.port << "," << metadataInTemp.sourceSocket.addr << "},{" << metadataInTemp.destinationSocket.port << "," << metadataInTemp.destinationSocket.addr << "}}" << endl; 
            return -1;
        }

        do
        {
            //read data
            outputFile >> std::hex >> dataTemp >> keepTemp >> lastTemp;
            while(rxDataOutApp_1.empty())
            {
                udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
                        rxDataOutApp_0, rxMetadataOutApp_0, rxPortApp0,
                        rxDataOutApp_1, rxMetadataOutApp_1, rxPortApp1,
                        requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
                        requestPortOpenInApp_0,portOpenReplyOutApp_0,
                        requestPortOpenInApp_1,portOpenReplyOutApp_0,
                        txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
                        txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
                        txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
                        txDataOut, txMetadataOut, txLengthOut);
            }

            dataInTemp = rxDataOutApp_1.read();
            if(dataTemp != dataInTemp.data ||
                    keepTemp != dataInTemp.keep ||
                    lastTemp != dataInTemp.last)
            {
                cout << num_data << endl;
                cout << "Data error!" << endl;
                cout << "Expected: " << endl;
                cout << "{" << dataTemp << "," << keepTemp << "," << lastTemp << "}" << endl; 
                cout << "{" << dataInTemp.data << "," << dataInTemp.keep << "," << dataInTemp.last << "}" << endl; 
                return -1;
            }
        }  while(!dataInTemp.last);
        --num_data;

    }
    outputFile.close();
    //while(rxMetadataOutApp_0.empty())
    //udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, 
    //rxDataOutApp_0, rxMetadataOutApp_0,
    //rxDataOutApp_1, rxMetadataOutApp_1,
    //requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, 
    //requestPortOpenInApp_0,portOpenReplyOutApp_0,
    //requestPortOpenInApp_1,portOpenReplyOutApp_0,
    //txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, 
    //txDataInApp_0, txMetadataInApp_0, txLengthInApp_0,
    //txDataInApp_1, txMetadataInApp_1, txLengthInApp_1,
    //txDataOut, txMetadataOut, txLengthOut);
    //metadata rxMetadataOutAppTest = rxMetadataOutApp_0.read();

    //cout << rxMetadataOutAppTest.destinationSocket.port << endl; 
    //requestPortOpenInDhcp.write(68);
    //for (uint8_t i=0;i<10;++i) {
    //udpAppMux(rxDataIn, rxMetadataIn, rxDataOutDhcp, rxMetadataOutDhcp, rxDataOutApp, rxMetadataOutApp,
    //requestPortOpenOut, portOpenReplyIn, requestPortOpenInDhcp, portOpenReplyOutDhcp, requestPortOpenInApp,portOpenReplyOutApp,
    //txDataInDhcp, txMetadataInDhcp, txLengthInDhcp, txDataInApp, txMetadataInApp, txLengthInApp,
    //txDataOut, txMetadataOut, txLengthOut);
    //if (!requestPortOpenOut.empty()) {
    //ap_uint<16> portToOpen = requestPortOpenOut.read();
    //cout << hex << portToOpen << endl;
    //portOpenReplyIn.write(true);
    //}
    //if (!portOpenReplyOutDhcp.empty()) {
    //bool portOpened = portOpenReplyOutDhcp.read();
    //cout << portOpened << endl;
    //}
    //}
    inputFile.close();
    outputFile.close();
    return 0;
}
