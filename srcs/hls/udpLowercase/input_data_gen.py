import random, string

def main():
    random.seed(0)
    inputDataFile = open("cap_in.dat", "w")
    outputGoldenDataFile = open("cap_out_golden.dat", "w")
    num_packets = 10
    inputDataFile.write(str(num_packets) + "\n")
    for i in range(num_packets):
        packet_length = random.randint(2,6)
        for j in range(packet_length):
            randomString = ''.join(random.choice(string.ascii_lowercase +  string.ascii_uppercase + string.digits) for _ in range(8))
            last = 1 if (random.randint(1, 100) > 75 or i == 99 or j == packet_length-1) else 0
            print randomString
            print ''.join("{:02x}".format(ord(c)) for c in randomString) , last
            inputDataFile.write(''.join("{:02x}".format(ord(c)) for c in randomString) + ' ' + str(last) + "\n")
            outputGoldenDataFile.write(''.join("{:02x}".format(ord(c)) for c in randomString.lower()) + ' ' + str(last) + "\n")
    inputDataFile.close()
    outputGoldenDataFile.close()

if __name__ == "__main__":
    main()
