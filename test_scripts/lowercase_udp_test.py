#!/usr/bin/python
import socket
from socket import AF_INET, SOCK_DGRAM
import sys
import datetime

def main():
    hostIPAddr = '1.1.1.1'
    port        = 642

    print "Testing Parameters:"
    print "\tip = ", hostIPAddr
    print "\tport = ", port

    s = socket.socket(AF_INET, SOCK_DGRAM)
    s.settimeout(2);
    while True:
        message = raw_input('Input sentence:')
        while True:
            s.sendto(message, (hostIPAddr, port))
            try:
                modifiedMessage, serverAddress = s.recvfrom(2048)
                packet_sent_succesfully = True
                break;
            except socket.timeout:
                print "Timed out. Trying again"
        
        print modifiedMessage

if __name__ == '__main__':
    main()



