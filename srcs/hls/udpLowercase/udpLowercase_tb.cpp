/************************************************
Copyright (c) 2016, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors 
may be used to endorse or promote products derived from this software 
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.// Copyright (c) 2015 Xilinx, Inc.
************************************************/
#include "udpLowercase.hpp"

using namespace std;

int main() {
	stream<axiWord>      	rxDataIn("rxDataIn");
    stream<metadata>     	rxMetadataIn("rxMetadataIn");
	stream<ap_uint<16> >    requestPortOpenOut("requestPortOpenOut");
	stream<bool > 			portOpenReplyIn("portOpenReplyIn");
	stream<axiWord> 		txDataOut("txDataOut");
	stream<metadata> 		txMetadataOut("txMetadataOut");
	stream<ap_uint<16> > 	txLengthOut("txLengthOut");
    ap_uint<16>             lbRxPort;
	axiWord inData;

	ifstream inputFile;
	ifstream outputFile;

	static ap_uint<32> ipAddress = 0x01010101;

	inputFile.open("../../../../cap_in.dat");
	if (!inputFile) {
		cout << "Error: could not open test input file." << endl;
		return -1;
	}

	outputFile.open("../../../../cap_out_golden.dat");
	if (!outputFile) {
		cout << "Error: could not open test output file." << endl;
		return -1;
	}

	uint32_t count = 0;
	uint16_t keepTemp;
	uint64_t dataTemp;
	uint16_t lastTemp;
    //Get port from UDP
	for (uint32_t i=0;i<1000;++i) {
		udpLowercase(rxDataIn, rxMetadataIn, requestPortOpenOut, portOpenReplyIn, txDataOut, txMetadataOut, txLengthOut, lbRxPort);
		if (!requestPortOpenOut.empty()) {
			requestPortOpenOut.read();
			portOpenReplyIn.write(true);
		}
	}

    uint32_t num_packets;
    inputFile >> std::dec >> num_packets;

    for(int i = 0; i < num_packets; ++i)
    {
        //fill input fifos with input data
        while(rxMetadataIn.full());
        metadata tempMetadata = {{0x0056, 0x0A0A0A0A}, {0x8000, 0x01010101}};
        rxMetadataIn.write(tempMetadata);

        do {
            inputFile >> std::hex >> dataTemp >> lastTemp;
            inData.data = dataTemp;
            inData.last = lastTemp;
            if (lastTemp) 
                inData.keep = 0x3F;
            else
                inData.keep = 0xFF;

            while(rxDataIn.full());
            rxDataIn.write(inData);
        } while(!lastTemp);
        
        //wait for output data
        while(txMetadataOut.empty())
            udpLowercase(rxDataIn, rxMetadataIn, requestPortOpenOut, portOpenReplyIn, txDataOut, txMetadataOut, txLengthOut, lbRxPort);
        tempMetadata = txMetadataOut.read();

        do
        {
            axiWord goldenDataOut;
            outputFile >> std::hex >> goldenDataOut.data >> goldenDataOut.last;
            while(txDataOut.empty())
                udpLowercase(rxDataIn, rxMetadataIn, requestPortOpenOut, portOpenReplyIn, txDataOut, txMetadataOut, txLengthOut, lbRxPort);
            inData = txDataOut.read();
            if(inData.last != goldenDataOut.last || inData.data != goldenDataOut.data)
            {
                cout << "ERROR!" << endl;
                cout << "expected " << std::hex << setw(16) << goldenDataOut.data << " " << goldenDataOut.last << endl; 
                cout << "got      " << std::hex << setw(16) << inData.data << " " << inData.last << endl; 
                inputFile.close();
                outputFile.close();
                return -1;
            }

                cout << std::hex << setw(16) << inData.data << " " << inData.last << endl; 
        } while(!inData.last);
    }

	inputFile.close();
	outputFile.close();
	return 0;
}

