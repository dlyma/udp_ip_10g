############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2016 Xilinx, Inc. All Rights Reserved.
############################################################
open_project udpLowercase_prj
set_top udpLowercase
add_files udpLowercase.cpp
add_files -tb udpLowercase_tb.cpp
open_solution "solution1"
set_part {xcku115-flva1517-2-e}
create_clock -period 6.66 -name default
#source "./udpLowercase_prj/solution1/directives.tcl"
#csim_design -clean -setup
csynth_design
#cosim_design
export_design -rtl verilog -format ip_catalog -description "This test app converts incoming strings to lowercase" -vendor "xilinx.labs" -version "1.10" -display_name "10G UDP Lowercase TestApp module "
exit
