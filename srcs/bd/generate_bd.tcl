
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2016.3
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   common::send_msg_id "BD_TCL-1002" "WARNING" "This script was generated using Vivado <$scripts_vivado_version> without IP versions in the create_bd_cell commands, but is now being run in <$current_vivado_version> of Vivado. There may have been major IP version changes between Vivado <$scripts_vivado_version> and <$current_vivado_version>, which could impact the parameter settings of the IPs."

}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcku115-flva1517-2-e
}


# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: udp_ip_wrapper
proc create_hier_cell_udp_ip_wrapper { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" create_hier_cell_udp_ip_wrapper() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 AXIS_S_Stream
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 AXI_M_Stream
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 portOpenReplyIn
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 portOpenReplyOutDhcp
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 requestPortOpenInDhcp
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 requestPortOpenOut
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 rxDataIn
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 rxDataOutDhcp
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 rxMetadataIn
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 rxMetadataOutDhcp
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 txDataInDhcp
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 txDataOut
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 txLengthInDhcp
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 txLengthOut
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 txMetadataInDhcp
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 txMetadataOut

  # Create pins
  create_bd_pin -dir I -type clk aclk
  create_bd_pin -dir I aresetn
  create_bd_pin -dir I -from 0 -to 0 dhcpEnable_V
  create_bd_pin -dir O -from 31 -to 0 dhcpIpAddressOut_V
  create_bd_pin -dir I -from 47 -to 0 -type data myMacAddress_V
  create_bd_pin -dir I -from 31 -to 0 -type data regIpAddress_V

  # Create instance: arpServerWrapper_0, and set properties
  set arpServerWrapper_0 [ create_bd_cell -type ip -vlnv user.org:user:arpServerWrapper arpServerWrapper_0 ]

  # Create instance: axis_interconnect_2to1, and set properties
  set axis_interconnect_2to1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect axis_interconnect_2to1 ]
  set_property -dict [ list \
CONFIG.ARB_ON_MAX_XFERS {0} \
CONFIG.ARB_ON_TLAST {1} \
CONFIG.M00_HAS_REGSLICE {1} \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {2} \
CONFIG.S00_HAS_REGSLICE {1} \
CONFIG.S01_HAS_REGSLICE {1} \
CONFIG.S02_HAS_REGSLICE {1} \
 ] $axis_interconnect_2to1

  # Create instance: axis_interconnect_3to1, and set properties
  set axis_interconnect_3to1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect axis_interconnect_3to1 ]
  set_property -dict [ list \
CONFIG.ARB_ON_TLAST {1} \
CONFIG.NUM_MI {1} \
CONFIG.NUM_SI {2} \
CONFIG.S00_HAS_REGSLICE {1} \
CONFIG.S01_HAS_REGSLICE {1} \
CONFIG.S02_HAS_REGSLICE {1} \
 ] $axis_interconnect_3to1

  # Create instance: axis_register_arp_in_slice, and set properties
  set axis_register_arp_in_slice [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice axis_register_arp_in_slice ]

  # Create instance: axis_register_icmp_in_slice, and set properties
  set axis_register_icmp_in_slice [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice axis_register_icmp_in_slice ]

  # Create instance: dhcp_client_0, and set properties
  set dhcp_client_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:dhcp_client dhcp_client_0 ]

  # Create instance: gateway, and set properties
  set gateway [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant gateway ]
  set_property -dict [ list \
CONFIG.CONST_VAL {16843009} \
CONFIG.CONST_WIDTH {32} \
 ] $gateway

  # Create instance: gnd, and set properties
  set gnd [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant gnd ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
 ] $gnd

  # Create instance: icmp_server_0, and set properties
  set icmp_server_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:icmp_server icmp_server_0 ]

  # Create instance: ip_handler_0, and set properties
  set ip_handler_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:ip_handler ip_handler_0 ]

  # Create instance: mac_ip_encode_0, and set properties
  set mac_ip_encode_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:mac_ip_encode mac_ip_encode_0 ]

  # Create instance: subnet, and set properties
  set subnet [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant subnet ]
  set_property -dict [ list \
CONFIG.CONST_VAL {16777215} \
CONFIG.CONST_WIDTH {32} \
 ] $subnet

  # Create instance: udp_0, and set properties
  set udp_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:udp udp_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant xlconstant_0 ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {16} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net AXIS_S_Stream_1 [get_bd_intf_pins AXIS_S_Stream] [get_bd_intf_pins ip_handler_0/dataIn]
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins portOpenReplyIn] [get_bd_intf_pins udp_0/confirmPortStatus]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins rxMetadataIn] [get_bd_intf_pins udp_0/inputPathOutputMetadata]
  connect_bd_intf_net -intf_net S01_AXIS_1 [get_bd_intf_pins axis_interconnect_2to1/S01_AXIS] [get_bd_intf_pins mac_ip_encode_0/dataOut]
  connect_bd_intf_net -intf_net arpServerWrapper_0_axi_arp_to_arp_slice [get_bd_intf_pins arpServerWrapper_0/axi_arp_to_arp_slice] [get_bd_intf_pins axis_interconnect_2to1/S00_AXIS]
  connect_bd_intf_net -intf_net arpServerWrapper_0_axis_arp_lookup_reply [get_bd_intf_pins arpServerWrapper_0/axis_arp_lookup_reply] [get_bd_intf_pins mac_ip_encode_0/arpTableIn_V]
  connect_bd_intf_net -intf_net axis_interconnect_2to1_M00_AXIS [get_bd_intf_pins AXI_M_Stream] [get_bd_intf_pins axis_interconnect_2to1/M00_AXIS]
  connect_bd_intf_net -intf_net axis_interconnect_3to1_M00_AXIS [get_bd_intf_pins axis_interconnect_3to1/M00_AXIS] [get_bd_intf_pins mac_ip_encode_0/dataIn]
  connect_bd_intf_net -intf_net axis_register_arp_in_slice_M_AXIS [get_bd_intf_pins arpServerWrapper_0/axi_arp_slice_to_arp] [get_bd_intf_pins axis_register_arp_in_slice/M_AXIS]
  connect_bd_intf_net -intf_net axis_register_icmp_in_slice_M_AXIS [get_bd_intf_pins axis_register_icmp_in_slice/M_AXIS] [get_bd_intf_pins icmp_server_0/dataIn]
  connect_bd_intf_net -intf_net dhcp_client_0_m_axis_open_port [get_bd_intf_pins requestPortOpenInDhcp] [get_bd_intf_pins dhcp_client_0/m_axis_open_port]
  connect_bd_intf_net -intf_net dhcp_client_0_m_axis_tx_data [get_bd_intf_pins txDataInDhcp] [get_bd_intf_pins dhcp_client_0/m_axis_tx_data]
  connect_bd_intf_net -intf_net dhcp_client_0_m_axis_tx_length [get_bd_intf_pins txLengthInDhcp] [get_bd_intf_pins dhcp_client_0/m_axis_tx_length]
  connect_bd_intf_net -intf_net dhcp_client_0_m_axis_tx_metadata [get_bd_intf_pins txMetadataInDhcp] [get_bd_intf_pins dhcp_client_0/m_axis_tx_metadata]
  connect_bd_intf_net -intf_net icmp_server_0_dataOut [get_bd_intf_pins axis_interconnect_3to1/S00_AXIS] [get_bd_intf_pins icmp_server_0/dataOut]
  connect_bd_intf_net -intf_net ip_handler_0_ARPdataOut [get_bd_intf_pins axis_register_arp_in_slice/S_AXIS] [get_bd_intf_pins ip_handler_0/ARPdataOut]
  connect_bd_intf_net -intf_net ip_handler_0_ICMPdataOut [get_bd_intf_pins axis_register_icmp_in_slice/S_AXIS] [get_bd_intf_pins ip_handler_0/ICMPdataOut]
  connect_bd_intf_net -intf_net ip_handler_0_ICMPexpDataOut [get_bd_intf_pins icmp_server_0/ttlIn] [get_bd_intf_pins ip_handler_0/ICMPexpDataOut]
  connect_bd_intf_net -intf_net ip_handler_0_UDPdataOut [get_bd_intf_pins ip_handler_0/UDPdataOut] [get_bd_intf_pins udp_0/inputPathInData]
  connect_bd_intf_net -intf_net mac_ip_encode_0_arpTableOut_V_V [get_bd_intf_pins arpServerWrapper_0/axis_arp_lookup_request] [get_bd_intf_pins mac_ip_encode_0/arpTableOut_V_V]
  connect_bd_intf_net -intf_net openPort_1 [get_bd_intf_pins requestPortOpenOut] [get_bd_intf_pins udp_0/openPort]
  connect_bd_intf_net -intf_net portOpenReplyOutDhcp_1 [get_bd_intf_pins portOpenReplyOutDhcp] [get_bd_intf_pins dhcp_client_0/s_axis_open_port_status]
  connect_bd_intf_net -intf_net rxDataOutDhcp_1 [get_bd_intf_pins rxDataOutDhcp] [get_bd_intf_pins dhcp_client_0/s_axis_rx_data]
  connect_bd_intf_net -intf_net rxMetadataOutDhcp_1 [get_bd_intf_pins rxMetadataOutDhcp] [get_bd_intf_pins dhcp_client_0/s_axis_rx_metadata]
  connect_bd_intf_net -intf_net txDataOut_1 [get_bd_intf_pins txDataOut] [get_bd_intf_pins udp_0/outputPathInData]
  connect_bd_intf_net -intf_net txLengthOut_1 [get_bd_intf_pins txLengthOut] [get_bd_intf_pins udp_0/outputpathInLength]
  connect_bd_intf_net -intf_net txMetadataOut_1 [get_bd_intf_pins txMetadataOut] [get_bd_intf_pins udp_0/outputPathInMetadata]
  connect_bd_intf_net -intf_net udp_0_inputPathPortUnreachable [get_bd_intf_pins icmp_server_0/udpIn] [get_bd_intf_pins udp_0/inputPathPortUnreachable]
  connect_bd_intf_net -intf_net udp_0_inputpathOutData [get_bd_intf_pins rxDataIn] [get_bd_intf_pins udp_0/inputpathOutData]
  connect_bd_intf_net -intf_net udp_0_outputPathOutData [get_bd_intf_pins axis_interconnect_3to1/S01_AXIS] [get_bd_intf_pins udp_0/outputPathOutData]

  # Create port connections
  connect_bd_net -net ap_clk_1 [get_bd_pins aclk] [get_bd_pins arpServerWrapper_0/aclk] [get_bd_pins axis_interconnect_2to1/ACLK] [get_bd_pins axis_interconnect_2to1/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_2to1/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_2to1/S01_AXIS_ACLK] [get_bd_pins axis_interconnect_3to1/ACLK] [get_bd_pins axis_interconnect_3to1/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_3to1/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_3to1/S01_AXIS_ACLK] [get_bd_pins axis_register_arp_in_slice/aclk] [get_bd_pins axis_register_icmp_in_slice/aclk] [get_bd_pins dhcp_client_0/aclk] [get_bd_pins icmp_server_0/ap_clk] [get_bd_pins ip_handler_0/ap_clk] [get_bd_pins mac_ip_encode_0/ap_clk] [get_bd_pins udp_0/aclk]
  connect_bd_net -net aresetn_1 [get_bd_pins aresetn] [get_bd_pins arpServerWrapper_0/aresetn] [get_bd_pins axis_interconnect_2to1/ARESETN] [get_bd_pins axis_interconnect_2to1/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_2to1/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_2to1/S01_AXIS_ARESETN] [get_bd_pins axis_interconnect_3to1/ARESETN] [get_bd_pins axis_interconnect_3to1/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_3to1/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_3to1/S01_AXIS_ARESETN] [get_bd_pins axis_register_arp_in_slice/aresetn] [get_bd_pins axis_register_icmp_in_slice/aresetn] [get_bd_pins dhcp_client_0/aresetn] [get_bd_pins icmp_server_0/ap_rst_n] [get_bd_pins ip_handler_0/ap_rst_n] [get_bd_pins mac_ip_encode_0/ap_rst_n] [get_bd_pins udp_0/aresetn]
  connect_bd_net -net dhcpEnable_V_1 [get_bd_pins dhcpEnable_V] [get_bd_pins dhcp_client_0/dhcpEnable_V]
  connect_bd_net -net dhcp_client_0_dhcpIpAddressOut_V [get_bd_pins dhcpIpAddressOut_V] [get_bd_pins dhcp_client_0/dhcpIpAddressOut_V]
  connect_bd_net -net gateway_dout [get_bd_pins gateway/dout] [get_bd_pins mac_ip_encode_0/regDefaultGateway_V]
  connect_bd_net -net gnd_dout [get_bd_pins axis_interconnect_2to1/S00_ARB_REQ_SUPPRESS] [get_bd_pins axis_interconnect_2to1/S01_ARB_REQ_SUPPRESS] [get_bd_pins axis_interconnect_3to1/S00_ARB_REQ_SUPPRESS] [get_bd_pins axis_interconnect_3to1/S01_ARB_REQ_SUPPRESS] [get_bd_pins gnd/dout] [get_bd_pins ip_handler_0/TCPdataOut_TREADY] [get_bd_pins udp_0/portRelease_TVALID]
  connect_bd_net -net myMacAddress_V_1 [get_bd_pins myMacAddress_V] [get_bd_pins arpServerWrapper_0/myMacAddress] [get_bd_pins dhcp_client_0/myMacAddress_V] [get_bd_pins ip_handler_0/myMacAddress_V] [get_bd_pins mac_ip_encode_0/myMacAddress_V]
  connect_bd_net -net regIpAddress_V_1 [get_bd_pins regIpAddress_V] [get_bd_pins arpServerWrapper_0/myIpAddress] [get_bd_pins dhcp_client_0/inputIpAddress_V] [get_bd_pins ip_handler_0/regIpAddress_V]
  connect_bd_net -net subnet_dout [get_bd_pins mac_ip_encode_0/regSubNetMask_V] [get_bd_pins subnet/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins udp_0/portRelease_TDATA] [get_bd_pins xlconstant_0/dout]

  # Perform GUI Layout
  regenerate_bd_layout -hierarchy [get_bd_cells /udp_ip_wrapper] -layout_string {
   DisplayTieOff: "1",
   commentid: "",
   guistr: "# # String gsaved with Nlview 6.6.5b  2016-09-06 bk=1.3687 VDI=39 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port requestPortOpenOut -pg 1 -y 560 -defaultsOSRD
preplace port requestPortOpenInDhcp -pg 1 -y 840 -defaultsOSRD
preplace port txLengthInDhcp -pg 1 -y 880 -defaultsOSRD
preplace port AXI_M_Stream -pg 1 -y 340 -defaultsOSRD
preplace port portOpenReplyOutDhcp -pg 1 -y 810 -defaultsOSRD
preplace port rxDataOutDhcp -pg 1 -y 830 -defaultsOSRD
preplace port txDataOut -pg 1 -y 580 -defaultsOSRD
preplace port txMetadataOut -pg 1 -y 600 -defaultsOSRD
preplace port rxDataIn -pg 1 -y 640 -defaultsOSRD
preplace port txDataInDhcp -pg 1 -y 860 -defaultsOSRD
preplace port txLengthOut -pg 1 -y 620 -defaultsOSRD
preplace port aclk -pg 1 -y 930 -defaultsOSRD
preplace port portOpenReplyIn -pg 1 -y 580 -defaultsOSRD
preplace port AXIS_S_Stream -pg 1 -y 340 -defaultsOSRD
preplace port rxMetadataIn -pg 1 -y 600 -defaultsOSRD
preplace port rxMetadataOutDhcp -pg 1 -y 850 -defaultsOSRD
preplace port txMetadataInDhcp -pg 1 -y 900 -defaultsOSRD
preplace port aresetn -pg 1 -y 950 -defaultsOSRD
preplace portBus myMacAddress_V -pg 1 -y 910 -defaultsOSRD
preplace portBus regIpAddress_V -pg 1 -y 890 -defaultsOSRD
preplace portBus dhcpEnable_V -pg 1 -y 870 -defaultsOSRD
preplace portBus dhcpIpAddressOut_V -pg 1 -y 920 -defaultsOSRD
preplace inst axis_interconnect_3to1 -pg 1 -lvl 4 -y 180 -defaultsOSRD
preplace inst mac_ip_encode_0 -pg 1 -lvl 5 -y 240 -defaultsOSRD
preplace inst arpServerWrapper_0 -pg 1 -lvl 6 -y 300 -defaultsOSRD
preplace inst xlconstant_0 -pg 1 -lvl 6 -y 480 -defaultsOSRD
preplace inst gnd -pg 1 -lvl 3 -y 440 -defaultsOSRD
preplace inst axis_register_icmp_in_slice -pg 1 -lvl 2 -y 220 -defaultsOSRD
preplace inst udp_0 -pg 1 -lvl 7 -y 640 -defaultsOSRD
preplace inst gateway -pg 1 -lvl 4 -y 460 -defaultsOSRD
preplace inst ip_handler_0 -pg 1 -lvl 1 -y 380 -defaultsOSRD
preplace inst dhcp_client_0 -pg 1 -lvl 7 -y 920 -defaultsOSRD
preplace inst subnet -pg 1 -lvl 4 -y 380 -defaultsOSRD
preplace inst axis_interconnect_2to1 -pg 1 -lvl 7 -y 340 -defaultsOSRD
preplace inst axis_register_arp_in_slice -pg 1 -lvl 5 -y 80 -defaultsOSRD
preplace inst icmp_server_0 -pg 1 -lvl 3 -y 320 -defaultsOSRD
preplace netloc Conn1 1 7 1 2930J
preplace netloc openPort_1 1 0 7 NJ 560 NJ 560 NJ 560 NJ 560 NJ 560 NJ 560 NJ
preplace netloc dhcp_client_0_m_axis_tx_metadata 1 7 1 2950J
preplace netloc dhcp_client_0_m_axis_open_port 1 7 1 2920J
preplace netloc Conn2 1 7 1 2940J
preplace netloc txDataOut_1 1 0 7 NJ 580 NJ 580 NJ 580 NJ 580 NJ 580 NJ 580 NJ
preplace netloc ip_handler_0_ICMPdataOut 1 1 1 510
preplace netloc gateway_dout 1 4 1 1430J
preplace netloc AXIS_S_Stream_1 1 0 1 NJ
preplace netloc gnd_dout 1 1 6 500J 660 NJ 660 1070 660 NJ 660 NJ 660 2380
preplace netloc txMetadataOut_1 1 0 7 NJ 600 NJ 600 NJ 600 NJ 600 NJ 600 NJ 600 NJ
preplace netloc ip_handler_0_UDPdataOut 1 1 6 510J 540 NJ 540 NJ 540 NJ 540 NJ 540 N
preplace netloc axis_register_arp_in_slice_M_AXIS 1 5 1 1950
preplace netloc axis_register_icmp_in_slice_M_AXIS 1 2 1 790
preplace netloc arpServerWrapper_0_axis_arp_lookup_reply 1 4 3 1450 10 NJ 10 2350
preplace netloc subnet_dout 1 4 1 1410J
preplace netloc rxMetadataOutDhcp_1 1 0 7 NJ 850 NJ 850 NJ 850 NJ 850 NJ 850 NJ 850 2360J
preplace netloc S01_AXIS_1 1 5 2 1920J 200 2390
preplace netloc dhcpEnable_V_1 1 0 7 NJ 870 NJ 870 NJ 870 NJ 870 NJ 870 NJ 870 2350J
preplace netloc portOpenReplyOutDhcp_1 1 0 7 NJ 810 NJ 810 NJ 810 NJ 810 NJ 810 NJ 810 2400J
preplace netloc rxDataOutDhcp_1 1 0 7 NJ 830 NJ 830 NJ 830 NJ 830 NJ 830 NJ 830 2380J
preplace netloc ip_handler_0_ICMPexpDataOut 1 1 2 N 370 760J
preplace netloc xlconstant_0_dout 1 6 1 2400
preplace netloc ap_clk_1 1 0 7 30 970 520 970 770 970 1060 970 1420 970 1920 970 2370
preplace netloc txLengthOut_1 1 0 7 NJ 620 NJ 620 NJ 620 NJ 620 NJ 620 NJ 620 NJ
preplace netloc icmp_server_0_dataOut 1 3 1 1050
preplace netloc dhcp_client_0_dhcpIpAddressOut_V 1 7 1 2960J
preplace netloc regIpAddress_V_1 1 0 7 10 890 NJ 890 NJ 890 NJ 890 NJ 890 1940 890 2340J
preplace netloc axis_interconnect_2to1_M00_AXIS 1 7 1 NJ
preplace netloc udp_0_inputPathPortUnreachable 1 2 6 790 790 NJ 790 NJ 790 NJ 790 NJ 790 2930
preplace netloc myMacAddress_V_1 1 0 7 20 910 NJ 910 NJ 910 NJ 910 1450 910 1930 910 2330J
preplace netloc aresetn_1 1 0 7 40 990 530 990 780 990 1080 990 1440 990 1950 990 2390
preplace netloc dhcp_client_0_m_axis_tx_data 1 7 1 2930J
preplace netloc arpServerWrapper_0_axi_arp_to_arp_slice 1 6 1 2360
preplace netloc ip_handler_0_ARPdataOut 1 1 4 500J 10 NJ 10 NJ 10 1420
preplace netloc axis_interconnect_3to1_M00_AXIS 1 4 1 N
preplace netloc udp_0_outputPathOutData 1 3 5 1090 800 NJ 800 NJ 800 NJ 800 2920
preplace netloc mac_ip_encode_0_arpTableOut_V_V 1 5 1 1920
preplace netloc udp_0_inputpathOutData 1 7 1 2940J
preplace netloc dhcp_client_0_m_axis_tx_length 1 7 1 2940J
levelinfo -pg 1 -10 280 660 920 1260 1710 2140 2670 2980 -top 0 -bot 1040
",
}

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: network_module
proc create_hier_cell_network_module { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" create_hier_cell_network_module() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M_AXIS
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S_AXIS

  # Create pins
  create_bd_pin -dir I aresetn
  create_bd_pin -dir I -type clk clk_100
  create_bd_pin -dir O -type clk clk_156
  create_bd_pin -dir O network_reset_done
  create_bd_pin -dir I refclk_n
  create_bd_pin -dir I refclk_p
  create_bd_pin -dir I -type rst reset
  create_bd_pin -dir I rxn
  create_bd_pin -dir I rxp
  create_bd_pin -dir O txn
  create_bd_pin -dir O txp

  # Create instance: axi_10g_ethernet_0, and set properties
  set axi_10g_ethernet_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_10g_ethernet axi_10g_ethernet_0 ]
  set_property -dict [ list \
CONFIG.Locations {X1Y16} \
CONFIG.Management_Interface {false} \
CONFIG.Statistics_Gathering {false} \
CONFIG.SupportLevel {1} \
CONFIG.autonegotiation {false} \
CONFIG.base_kr {BASE-R} \
CONFIG.fec {false} \
 ] $axi_10g_ethernet_0

  # Create instance: axis_register_slice_0, and set properties
  set axis_register_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice axis_register_slice_0 ]
  set_property -dict [ list \
CONFIG.TUSER_WIDTH {0} \
 ] $axis_register_slice_0

  # Create instance: axis_register_slice_1, and set properties
  set axis_register_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice axis_register_slice_1 ]
  set_property -dict [ list \
CONFIG.TUSER_WIDTH {0} \
 ] $axis_register_slice_1

  # Create instance: gnd, and set properties
  set gnd [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant gnd ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
 ] $gnd

  # Create instance: ifg_delay, and set properties
  set ifg_delay [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant ifg_delay ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {8} \
 ] $ifg_delay

  # Create instance: mac_config_vector, and set properties
  set mac_config_vector [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant mac_config_vector ]
  set_property -dict [ list \
CONFIG.CONST_VAL {22} \
CONFIG.CONST_WIDTH {80} \
 ] $mac_config_vector

  # Create instance: pcs_config_vector, and set properties
  set pcs_config_vector [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant pcs_config_vector ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {536} \
 ] $pcs_config_vector

  # Create instance: tx_ifg_tuser, and set properties
  set tx_ifg_tuser [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant tx_ifg_tuser ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {16} \
 ] $tx_ifg_tuser

  # Create instance: vcc, and set properties
  set vcc [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant vcc ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S_AXIS] [get_bd_intf_pins axis_register_slice_0/S_AXIS]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins M_AXIS] [get_bd_intf_pins axis_register_slice_1/M_AXIS]
  connect_bd_intf_net -intf_net axi_10g_ethernet_0_m_axis_rx [get_bd_intf_pins axi_10g_ethernet_0/m_axis_rx] [get_bd_intf_pins axis_register_slice_1/S_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_0_M_AXIS [get_bd_intf_pins axi_10g_ethernet_0/s_axis_tx] [get_bd_intf_pins axis_register_slice_0/M_AXIS]

  # Create port connections
  connect_bd_net -net Net [get_bd_pins aresetn] [get_bd_pins axi_10g_ethernet_0/rx_axis_aresetn] [get_bd_pins axi_10g_ethernet_0/tx_axis_aresetn] [get_bd_pins axis_register_slice_0/aresetn] [get_bd_pins axis_register_slice_1/aresetn]
  connect_bd_net -net axi_10g_ethernet_0_resetdone_out [get_bd_pins network_reset_done] [get_bd_pins axi_10g_ethernet_0/resetdone_out]
  connect_bd_net -net axi_10g_ethernet_0_txn [get_bd_pins txn] [get_bd_pins axi_10g_ethernet_0/txn]
  connect_bd_net -net axi_10g_ethernet_0_txp [get_bd_pins txp] [get_bd_pins axi_10g_ethernet_0/txp]
  connect_bd_net -net axi_10g_ethernet_0_txusrclk2_out [get_bd_pins clk_156] [get_bd_pins axi_10g_ethernet_0/txusrclk2_out] [get_bd_pins axis_register_slice_0/aclk] [get_bd_pins axis_register_slice_1/aclk]
  connect_bd_net -net config_vector_dout [get_bd_pins axi_10g_ethernet_0/mac_rx_configuration_vector] [get_bd_pins axi_10g_ethernet_0/mac_tx_configuration_vector] [get_bd_pins mac_config_vector/dout]
  connect_bd_net -net dclk_1 [get_bd_pins clk_100] [get_bd_pins axi_10g_ethernet_0/dclk]
  connect_bd_net -net gnd_dout [get_bd_pins axi_10g_ethernet_0/s_axis_pause_tvalid] [get_bd_pins axi_10g_ethernet_0/sim_speedup_control] [get_bd_pins axi_10g_ethernet_0/tx_fault] [get_bd_pins gnd/dout]
  connect_bd_net -net ifg_delay_dout [get_bd_pins axi_10g_ethernet_0/tx_ifg_delay] [get_bd_pins ifg_delay/dout]
  connect_bd_net -net pcs_config_vector_dout [get_bd_pins axi_10g_ethernet_0/pcs_pma_configuration_vector] [get_bd_pins pcs_config_vector/dout]
  connect_bd_net -net refclk_n_1 [get_bd_pins refclk_n] [get_bd_pins axi_10g_ethernet_0/refclk_n]
  connect_bd_net -net refclk_p_1 [get_bd_pins refclk_p] [get_bd_pins axi_10g_ethernet_0/refclk_p]
  connect_bd_net -net reset_1 [get_bd_pins reset] [get_bd_pins axi_10g_ethernet_0/reset]
  connect_bd_net -net rxn_1 [get_bd_pins rxn] [get_bd_pins axi_10g_ethernet_0/rxn]
  connect_bd_net -net rxp_1 [get_bd_pins rxp] [get_bd_pins axi_10g_ethernet_0/rxp]
  connect_bd_net -net tx_ifg_tuser_dout [get_bd_pins axi_10g_ethernet_0/s_axis_pause_tdata] [get_bd_pins tx_ifg_tuser/dout]
  connect_bd_net -net vcc_dout [get_bd_pins axi_10g_ethernet_0/signal_detect] [get_bd_pins vcc/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set led_l [ create_bd_port -dir O -from 2 -to 0 led_l ]
  set perst_n [ create_bd_port -dir I perst_n ]
  set refclk200 [ create_bd_port -dir I -type clk refclk200 ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {200000000} \
 ] $refclk200
  set refclk_n [ create_bd_port -dir I refclk_n ]
  set refclk_p [ create_bd_port -dir I refclk_p ]
  set rxn [ create_bd_port -dir I rxn ]
  set rxp [ create_bd_port -dir I rxp ]
  set sfp_tx_disable [ create_bd_port -dir O -from 1 -to 0 sfp_tx_disable ]
  set txn [ create_bd_port -dir O txn ]
  set txp [ create_bd_port -dir O txp ]
  set user_sw_l [ create_bd_port -dir I user_sw_l ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz clk_wiz_0 ]
  set_property -dict [ list \
CONFIG.CLKIN1_JITTER_PS {50.0} \
CONFIG.CLKOUT1_DRIVES {Buffer} \
CONFIG.CLKOUT1_JITTER {112.316} \
CONFIG.CLKOUT1_PHASE_ERROR {89.971} \
CONFIG.CLKOUT2_DRIVES {Buffer} \
CONFIG.CLKOUT3_DRIVES {Buffer} \
CONFIG.CLKOUT4_DRIVES {Buffer} \
CONFIG.CLKOUT5_DRIVES {Buffer} \
CONFIG.CLKOUT6_DRIVES {Buffer} \
CONFIG.CLKOUT7_DRIVES {Buffer} \
CONFIG.MMCM_CLKFBOUT_MULT_F {5.000} \
CONFIG.MMCM_CLKIN1_PERIOD {5.0} \
CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F {10.000} \
CONFIG.MMCM_DIVCLK_DIVIDE {1} \
CONFIG.USE_LOCKED {false} \
CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Need to retain value_src of defaults
  set_property -dict [ list \
CONFIG.CLKOUT1_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT2_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT3_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT4_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT5_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT6_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.CLKOUT7_DRIVES.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKIN2_PERIOD.VALUE_SRC {DEFAULT} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F.VALUE_SRC {DEFAULT} \
 ] $clk_wiz_0

  # Create instance: gnd, and set properties
  set gnd [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant gnd ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {1} \
 ] $gnd

  # Create instance: gnd1, and set properties
  set gnd1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant gnd1 ]
  set_property -dict [ list \
CONFIG.CONST_VAL {0} \
CONFIG.CONST_WIDTH {2} \
 ] $gnd1

  # Create instance: ip, and set properties
  set ip [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant ip ]
  set_property -dict [ list \
CONFIG.CONST_VAL {16843009} \
CONFIG.CONST_WIDTH {32} \
 ] $ip

  # Create instance: mac, and set properties
  set mac [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant mac ]
  set_property -dict [ list \
CONFIG.CONST_VAL {252462509656576} \
CONFIG.CONST_WIDTH {48} \
 ] $mac

  # Create instance: network_module
  create_hier_cell_network_module [current_bd_instance .] network_module

  # Create instance: proc_sys_reset_0, and set properties
  set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset proc_sys_reset_0 ]

  # Create instance: udpAppMux_0, and set properties
  set udpAppMux_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:udpAppMux udpAppMux_0 ]

  # Create instance: udpCapitalize_0, and set properties
  set udpCapitalize_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:udpCapitalize udpCapitalize_0 ]

  # Create instance: udpLowercase_0, and set properties
  set udpLowercase_0 [ create_bd_cell -type ip -vlnv xilinx.labs:hls:udpLowercase udpLowercase_0 ]

  # Create instance: udp_ip_wrapper
  create_hier_cell_udp_ip_wrapper [current_bd_instance .] udp_ip_wrapper

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic util_vector_logic_0 ]
  set_property -dict [ list \
CONFIG.C_SIZE {1} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic util_vector_logic_1 ]
  set_property -dict [ list \
CONFIG.C_OPERATION {not} \
CONFIG.C_SIZE {1} \
CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_1

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0 ]
  set_property -dict [ list \
CONFIG.NUM_PORTS {3} \
 ] $xlconcat_0

  # Create interface connections
  connect_bd_intf_net -intf_net network_module_M_AXIS [get_bd_intf_pins network_module/M_AXIS] [get_bd_intf_pins udp_ip_wrapper/AXIS_S_Stream]
  connect_bd_intf_net -intf_net portOpenReplyOutDhcp_1 [get_bd_intf_pins udpAppMux_0/portOpenReplyOutDhcp] [get_bd_intf_pins udp_ip_wrapper/portOpenReplyOutDhcp]
  connect_bd_intf_net -intf_net requestPortOpenOut_1 [get_bd_intf_pins udpAppMux_0/requestPortOpenOut] [get_bd_intf_pins udp_ip_wrapper/requestPortOpenOut]
  connect_bd_intf_net -intf_net rxDataOutDhcp_1 [get_bd_intf_pins udpAppMux_0/rxDataOutDhcp] [get_bd_intf_pins udp_ip_wrapper/rxDataOutDhcp]
  connect_bd_intf_net -intf_net rxMetadataOutDhcp_1 [get_bd_intf_pins udpAppMux_0/rxMetadataOutDhcp] [get_bd_intf_pins udp_ip_wrapper/rxMetadataOutDhcp]
  connect_bd_intf_net -intf_net txDataOut_1 [get_bd_intf_pins udpAppMux_0/txDataOut] [get_bd_intf_pins udp_ip_wrapper/txDataOut]
  connect_bd_intf_net -intf_net txLengthOut_1 [get_bd_intf_pins udpAppMux_0/txLengthOut] [get_bd_intf_pins udp_ip_wrapper/txLengthOut]
  connect_bd_intf_net -intf_net txMetadataOut_1 [get_bd_intf_pins udpAppMux_0/txMetadataOut] [get_bd_intf_pins udp_ip_wrapper/txMetadataOut]
  connect_bd_intf_net -intf_net udpAppMux_0_portOpenReplyOutApp0 [get_bd_intf_pins udpAppMux_0/portOpenReplyOutApp0] [get_bd_intf_pins udpLowercase_0/lbPortOpenReplyIn]
  connect_bd_intf_net -intf_net udpAppMux_0_portOpenReplyOutApp1 [get_bd_intf_pins udpAppMux_0/portOpenReplyOutApp1] [get_bd_intf_pins udpCapitalize_0/lbPortOpenReplyIn]
  connect_bd_intf_net -intf_net udpAppMux_0_rxDataOutApp0 [get_bd_intf_pins udpAppMux_0/rxDataOutApp0] [get_bd_intf_pins udpLowercase_0/lbRxDataIn]
  connect_bd_intf_net -intf_net udpAppMux_0_rxDataOutApp1 [get_bd_intf_pins udpAppMux_0/rxDataOutApp1] [get_bd_intf_pins udpCapitalize_0/lbRxDataIn]
  connect_bd_intf_net -intf_net udpAppMux_0_rxMetadataOutApp0 [get_bd_intf_pins udpAppMux_0/rxMetadataOutApp0] [get_bd_intf_pins udpLowercase_0/lbRxMetadataIn]
  connect_bd_intf_net -intf_net udpAppMux_0_rxMetadataOutApp1 [get_bd_intf_pins udpAppMux_0/rxMetadataOutApp1] [get_bd_intf_pins udpCapitalize_0/lbRxMetadataIn]
  connect_bd_intf_net -intf_net udpCapitalize_0_lbRequestPortOpenOut [get_bd_intf_pins udpAppMux_0/requestPortOpenInApp1] [get_bd_intf_pins udpCapitalize_0/lbRequestPortOpenOut]
  connect_bd_intf_net -intf_net udpCapitalize_0_lbTxDataOut [get_bd_intf_pins udpAppMux_0/txDataInApp1] [get_bd_intf_pins udpCapitalize_0/lbTxDataOut]
  connect_bd_intf_net -intf_net udpCapitalize_0_lbTxLengthOut [get_bd_intf_pins udpAppMux_0/txLengthInApp1] [get_bd_intf_pins udpCapitalize_0/lbTxLengthOut]
  connect_bd_intf_net -intf_net udpCapitalize_0_lbTxMetadataOut [get_bd_intf_pins udpAppMux_0/txMetadataInApp1] [get_bd_intf_pins udpCapitalize_0/lbTxMetadataOut]
  connect_bd_intf_net -intf_net udpLowercase_0_lbRequestPortOpenOut [get_bd_intf_pins udpAppMux_0/requestPortOpenInApp0] [get_bd_intf_pins udpLowercase_0/lbRequestPortOpenOut]
  connect_bd_intf_net -intf_net udpLowercase_0_lbTxDataOut [get_bd_intf_pins udpAppMux_0/txDataInApp0] [get_bd_intf_pins udpLowercase_0/lbTxDataOut]
  connect_bd_intf_net -intf_net udpLowercase_0_lbTxLengthOut [get_bd_intf_pins udpAppMux_0/txLengthInApp0] [get_bd_intf_pins udpLowercase_0/lbTxLengthOut]
  connect_bd_intf_net -intf_net udpLowercase_0_lbTxMetadataOut [get_bd_intf_pins udpAppMux_0/txMetadataInApp0] [get_bd_intf_pins udpLowercase_0/lbTxMetadataOut]
  connect_bd_intf_net -intf_net udp_ip_wrapper_AXI_M_Stream [get_bd_intf_pins network_module/S_AXIS] [get_bd_intf_pins udp_ip_wrapper/AXI_M_Stream]
  connect_bd_intf_net -intf_net udp_ip_wrapper_portOpenReplyIn [get_bd_intf_pins udpAppMux_0/portOpenReplyIn] [get_bd_intf_pins udp_ip_wrapper/portOpenReplyIn]
  connect_bd_intf_net -intf_net udp_ip_wrapper_requestPortOpenInDhcp [get_bd_intf_pins udpAppMux_0/requestPortOpenInDhcp] [get_bd_intf_pins udp_ip_wrapper/requestPortOpenInDhcp]
  connect_bd_intf_net -intf_net udp_ip_wrapper_rxDataIn [get_bd_intf_pins udpAppMux_0/rxDataIn] [get_bd_intf_pins udp_ip_wrapper/rxDataIn]
  connect_bd_intf_net -intf_net udp_ip_wrapper_rxMetadataIn [get_bd_intf_pins udpAppMux_0/rxMetadataIn] [get_bd_intf_pins udp_ip_wrapper/rxMetadataIn]
  connect_bd_intf_net -intf_net udp_ip_wrapper_txDataInDhcp [get_bd_intf_pins udpAppMux_0/txDataInDhcp] [get_bd_intf_pins udp_ip_wrapper/txDataInDhcp]
  connect_bd_intf_net -intf_net udp_ip_wrapper_txLengthInDhcp [get_bd_intf_pins udpAppMux_0/txLengthInDhcp] [get_bd_intf_pins udp_ip_wrapper/txLengthInDhcp]
  connect_bd_intf_net -intf_net udp_ip_wrapper_txMetadataInDhcp [get_bd_intf_pins udpAppMux_0/txMetadataInDhcp] [get_bd_intf_pins udp_ip_wrapper/txMetadataInDhcp]

  # Create port connections
  connect_bd_net -net aresetn_1 [get_bd_pins network_module/aresetn] [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins udpAppMux_0/aresetn] [get_bd_pins udpCapitalize_0/aresetn] [get_bd_pins udpLowercase_0/aresetn] [get_bd_pins udp_ip_wrapper/aresetn]
  connect_bd_net -net clk_100_1 [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins network_module/clk_100]
  connect_bd_net -net dhcpEnable_V_1 [get_bd_pins gnd/dout] [get_bd_pins udp_ip_wrapper/dhcpEnable_V]
  connect_bd_net -net gnd1_dout [get_bd_ports sfp_tx_disable] [get_bd_pins gnd1/dout]
  connect_bd_net -net mac_dout [get_bd_pins mac/dout] [get_bd_pins udp_ip_wrapper/myMacAddress_V]
  connect_bd_net -net network_module_network_reset_done [get_bd_pins network_module/network_reset_done] [get_bd_pins util_vector_logic_0/Op2] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net network_module_txn [get_bd_ports txn] [get_bd_pins network_module/txn]
  connect_bd_net -net network_module_txp [get_bd_ports txp] [get_bd_pins network_module/txp]
  connect_bd_net -net network_module_txusrclk2_out [get_bd_pins network_module/clk_156] [get_bd_pins proc_sys_reset_0/slowest_sync_clk] [get_bd_pins udpAppMux_0/aclk] [get_bd_pins udpCapitalize_0/aclk] [get_bd_pins udpLowercase_0/aclk] [get_bd_pins udp_ip_wrapper/aclk]
  connect_bd_net -net psert_n_1 [get_bd_ports perst_n] [get_bd_pins util_vector_logic_0/Op1] [get_bd_pins util_vector_logic_1/Op1] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net refclk200_1 [get_bd_ports refclk200] [get_bd_pins clk_wiz_0/clk_in1]
  connect_bd_net -net refclk_n_1 [get_bd_ports refclk_n] [get_bd_pins network_module/refclk_n]
  connect_bd_net -net refclk_p_1 [get_bd_ports refclk_p] [get_bd_pins network_module/refclk_p]
  connect_bd_net -net regIpAddress_V_1 [get_bd_pins ip/dout] [get_bd_pins udp_ip_wrapper/regIpAddress_V]
  connect_bd_net -net rxn_1 [get_bd_ports rxn] [get_bd_pins network_module/rxn]
  connect_bd_net -net rxp_1 [get_bd_ports rxp] [get_bd_pins network_module/rxp]
  connect_bd_net -net udpCapitalize_0_lbRxPort_V [get_bd_pins udpAppMux_0/rxPortApp1_V] [get_bd_pins udpCapitalize_0/lbRxPort_V]
  connect_bd_net -net udpLowercase_0_lbRxPort_V [get_bd_pins udpAppMux_0/rxPortApp0_V] [get_bd_pins udpLowercase_0/lbRxPort_V]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins proc_sys_reset_0/ext_reset_in] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins network_module/reset] [get_bd_pins util_vector_logic_1/Res] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net xlconcat_0_dout [get_bd_ports led_l] [get_bd_pins xlconcat_0/dout]

  # Create address segments

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.6.5b  2016-09-06 bk=1.3687 VDI=39 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port refclk_n -pg 1 -y 990 -defaultsOSRD
preplace port refclk_p -pg 1 -y 1070 -defaultsOSRD
preplace port txn -pg 1 -y 1040 -defaultsOSRD
preplace port perst_n -pg 1 -y 1170 -defaultsOSRD
preplace port txp -pg 1 -y 1020 -defaultsOSRD
preplace port user_sw_l -pg 1 -y 10 -defaultsOSRD
preplace port rxn -pg 1 -y 1010 -defaultsOSRD
preplace port refclk200 -pg 1 -y 1030 -defaultsOSRD
preplace port rxp -pg 1 -y 1050 -defaultsOSRD
preplace portBus led_l -pg 1 -y 1190 -defaultsOSRD
preplace portBus sfp_tx_disable -pg 1 -y 1090 -defaultsOSRD
preplace inst udpLowercase_0 -pg 1 -lvl 3 -y 410 -defaultsOSRD
preplace inst gnd1 -pg 1 -lvl 7 -y 1090 -defaultsOSRD
preplace inst udpCapitalize_0 -pg 1 -lvl 3 -y 230 -defaultsOSRD
preplace inst gnd -pg 1 -lvl 4 -y 860 -defaultsOSRD -resize 100 60
preplace inst udpAppMux_0 -pg 1 -lvl 4 -y 250 -defaultsOSRD
preplace inst xlconcat_0 -pg 1 -lvl 7 -y 1190 -defaultsOSRD
preplace inst util_vector_logic_0 -pg 1 -lvl 1 -y 960 -defaultsOSRD
preplace inst proc_sys_reset_0 -pg 1 -lvl 2 -y 620 -defaultsOSRD
preplace inst util_vector_logic_1 -pg 1 -lvl 5 -y 1110 -defaultsOSRD
preplace inst udp_ip_wrapper -pg 1 -lvl 5 -y 740 -defaultsOSRD
preplace inst clk_wiz_0 -pg 1 -lvl 5 -y 980 -defaultsOSRD
preplace inst mac -pg 1 -lvl 4 -y 780 -defaultsOSRD
preplace inst network_module -pg 1 -lvl 6 -y 1020 -defaultsOSRD
preplace inst ip -pg 1 -lvl 4 -y 700 -defaultsOSRD
preplace netloc udpLowercase_0_lbRxPort_V 1 3 1 1170
preplace netloc udp_ip_wrapper_rxDataIn 1 3 3 1230 490 NJ 490 2290
preplace netloc txDataOut_1 1 4 1 1780
preplace netloc gnd1_dout 1 7 1 NJ
preplace netloc network_module_txusrclk2_out 1 1 6 300 910 640 910 1180 910 1780 910 2260J 890 2570
preplace netloc udpLowercase_0_lbRequestPortOpenOut 1 3 1 1110
preplace netloc udpCapitalize_0_lbRequestPortOpenOut 1 3 1 1120
preplace netloc udpLowercase_0_lbTxLengthOut 1 3 1 1150
preplace netloc udpCapitalize_0_lbRxPort_V 1 3 1 1140
preplace netloc rxp_1 1 0 6 NJ 1050 NJ 1050 NJ 1050 NJ 1050 NJ 1050 2230J
preplace netloc udpAppMux_0_rxDataOutApp0 1 2 3 660 10 NJ 10 1730
preplace netloc udp_ip_wrapper_rxMetadataIn 1 3 3 1220 530 NJ 530 2250
preplace netloc udp_ip_wrapper_txMetadataInDhcp 1 3 3 1250 520 NJ 520 2230
preplace netloc txMetadataOut_1 1 4 1 1760
preplace netloc network_module_M_AXIS 1 4 3 1830 470 NJ 470 2580
preplace netloc mac_dout 1 4 1 1720J
preplace netloc rxn_1 1 0 6 10J 1020 NJ 1020 NJ 1020 NJ 1020 1820J 1030 2290J
preplace netloc requestPortOpenOut_1 1 4 1 1810
preplace netloc udpAppMux_0_rxDataOutApp1 1 2 3 690 20 NJ 20 1720
preplace netloc udp_ip_wrapper_txDataInDhcp 1 3 3 1240 510 NJ 510 2260
preplace netloc udpCapitalize_0_lbTxMetadataOut 1 3 1 1120
preplace netloc util_vector_logic_0_Res 1 1 1 290
preplace netloc udpLowercase_0_lbTxDataOut 1 3 1 1130
preplace netloc refclk200_1 1 0 5 NJ 1030 NJ 1030 NJ 1030 NJ 1030 1750J
preplace netloc rxMetadataOutDhcp_1 1 4 1 1790
preplace netloc dhcpEnable_V_1 1 4 1 NJ
preplace netloc refclk_n_1 1 0 6 20J 1040 NJ 1040 NJ 1040 NJ 1040 NJ 1040 2240J
preplace netloc portOpenReplyOutDhcp_1 1 4 1 1820
preplace netloc rxDataOutDhcp_1 1 4 1 1800
preplace netloc xlconcat_0_dout 1 7 1 NJ
preplace netloc clk_100_1 1 5 1 2300J
preplace netloc network_module_txn 1 6 2 NJ 1020 2790J
preplace netloc txLengthOut_1 1 4 1 1770
preplace netloc regIpAddress_V_1 1 4 1 1740J
preplace netloc psert_n_1 1 0 7 30 1170 NJ 1170 NJ 1170 NJ 1170 1780 1170 NJ 1170 NJ
preplace netloc udp_ip_wrapper_requestPortOpenInDhcp 1 3 3 1210 560 NJ 560 2240
preplace netloc udpCapitalize_0_lbTxDataOut 1 3 1 N
preplace netloc aresetn_1 1 2 4 650 920 1190 920 1820 920 2270J
preplace netloc network_module_txp 1 6 2 NJ 1000 2800J
preplace netloc udp_ip_wrapper_portOpenReplyIn 1 3 3 1200 550 NJ 550 2270
preplace netloc udpAppMux_0_portOpenReplyOutApp0 1 2 3 680 570 NJ 570 1750
preplace netloc udpAppMux_0_rxMetadataOutApp0 1 2 3 690 500 NJ 500 1730
preplace netloc udp_ip_wrapper_AXI_M_Stream 1 5 1 2300
preplace netloc util_vector_logic_1_Res 1 5 2 2300J 1210 N
preplace netloc udpAppMux_0_portOpenReplyOutApp1 1 2 3 680 0 NJ 0 1740
preplace netloc udpAppMux_0_rxMetadataOutApp1 1 2 3 670 540 NJ 540 1720
preplace netloc udpLowercase_0_lbTxMetadataOut 1 3 1 1160
preplace netloc network_module_network_reset_done 1 0 7 40 1190 NJ 1190 NJ 1190 NJ 1190 NJ 1190 NJ 1190 2580
preplace netloc udpCapitalize_0_lbTxLengthOut 1 3 1 1160
preplace netloc udp_ip_wrapper_txLengthInDhcp 1 3 3 1260 480 NJ 480 2280
preplace netloc refclk_p_1 1 0 6 NJ 1070 NJ 1070 NJ 1070 NJ 1070 1770J 1060 2300J
levelinfo -pg 1 -10 170 470 900 1490 2030 2440 2700 2820 -top -10 -bot 1260
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


