############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2016 Xilinx, Inc. All Rights Reserved.
############################################################
open_project udpAppMux_prj
set_top udpAppMux
add_files udpAppMux.cpp
add_files -tb udpAppMux_tb.cpp
open_solution "solution1"
set_part {xcku115-flva1517-2-e}
create_clock -period 6.66 -name default
config_rtl -reset all -reset_async
#csim_design -clean -compiler gcc -setup
csynth_design
#cosim_design
export_design -rtl verilog -format ip_catalog -description "This module enables the connection of both the DHCP server and an additional application to the UDP offload engine, granted the DHCP server uses the standard ports." -vendor "xilinx.labs" -version "1.1" -display_name "UDP App Multiplexer for DHCP"
exit
