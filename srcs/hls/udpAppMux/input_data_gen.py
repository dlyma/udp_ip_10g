import random, string

def main():
    random.seed(0)
    
    valid_ports = [0x0044, 0x0281, 0x0282]

    data_out = [[], [], []]
    data_out_size = [0,0,0]
    inputDataFile = open("in_rx.dat", "w")
    for i in range(100):
        dest_port   = random.choice(valid_ports)
        dest_addr   = random.randint(0, pow(2,32)-1)
        
        src_port    = random.randint(0, pow(2,16)-1)
        src_addr    = random.randint(0, pow(2,32)-1)
        metadata =  format(src_port, '04x') + " " + format(src_addr, '08x') + " " + format(dest_port, '04x') + " " + format(dest_addr, '08x')
        print metadata
        inputDataFile.write(metadata + '\n')
        data_out_index = 0
        for j in range(3):
            if dest_port == valid_ports[j]:
                data_out[j].append(metadata)
                data_out_index = j
                data_out_size[j] += 1
                break

        last = 0
        while(not last):
            data = random.randint(0, pow(2,32)-1) << 32 | random.randint(0, pow(2,32)-1)
            last = 1 if random.randint(0,100) > 80 else 0
            keep = 0xff if last==0 else 0x3f
            data_in = format(data, '016x') + " " + format(keep, '02x') + " " + format(last, '01x') 
            print data_in
            inputDataFile.write(data_in + '\n')
            data_out[data_out_index].append(data_in)
    
    inputDataFile.close()

    for i in range(3):
        outputGoldenDataFile = open("out_golden_port" + str(i) + ".dat", "w")
        print "PORT" + str(i)
        outputGoldenDataFile.write(str(data_out_size[i]) + '\n')
        for j in range(len(data_out[i])):
            print(data_out[i][j])
            outputGoldenDataFile.write(data_out[i][j] + '\n')
        outputGoldenDataFile.close()
            
    # for i in range(100):
        # randomString = ''.join(random.choice(string.ascii_lowercase +  string.ascii_uppercase + string.digits) for _ in range(8))
        # last = 1 if (random.randint(1, 100) > 75 or i == 99) else 0
        # # print ''.join("{:02x}".format(ord(c)) for c in randomString) , last
        # inputDataFile.write(''.join("{:02x}".format(ord(c)) for c in randomString) + ' ' + str(last) + "\n")
        # outputGoldenDataFile.write(''.join("{:02x}".format(ord(c)) for c in randomString.upper()) + ' ' + str(last) + "\n")
    # outputGoldenDataFile.close()

if __name__ == "__main__":
    main()
