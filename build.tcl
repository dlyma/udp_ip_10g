#Description: This scripts builds a vivado project using your source files
#Your folder structure must be set to this:
#├── build.tcl
#└── srcs
    #├── bd
    #├── constraints
    #├── ip
    #│   ├── config
    #│   ├── dcp
    #│   └── repo
    #└── rtl

#set your project name here
set proj_name "udp_ip_system"
set root_dir [pwd]
set proj_dir $root_dir/build
set src_dir $root_dir/srcs/rtl
set ip_repo_dir $root_dir/srcs/ip/repo
set ip_config_dir $root_dir/srcs/ip/config
set ip_dcp_dir $root_dir/srcs/ip/dcp
set constraints_dir $root_dir/srcs/constraints
set bd_dir $root_dir/srcs/bd

# Create project
create_project -force $proj_name $proj_dir

# Set project properties
set obj [get_projects $proj_name]

#set your device 
set_property part xcku115-flva1517-2-e $obj
set_property "target_language" "Verilog" $obj

set_property IP_REPO_PATHS $ip_repo_dir [current_fileset]
update_ip_catalog

# Add sources
add_files -quiet $src_dir

#add xci files
add_files -quiet [glob -nocomplain $ip_config_dir/*/*.xci]
add_files -quiet [glob -nocomplain $ip_dcp_dir/*.dcp]
#add constraints
add_files -quiet  -norecurse -fileset constrs_1 $constraints_dir

#upgrade ips, disable this if there are incompatibilities
#upgrade_ip [get_ips]

if {[llength [glob -nocomplain $bd_dir/generate_bd.tcl]] > 0} {
    source $bd_dir/generate_bd.tcl
    make_wrapper -top -import -force [get_files design_1.bd]
}
#start_gui
#close_project

