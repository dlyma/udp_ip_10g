#include "udpCapitalize.hpp"

#define PORT 0x0281
void rxPath(stream<axiWord>&       lbRxDataIn,
        	stream<metadata>&      lbRxMetadataIn,
        	stream<ap_uint<16> >&  lbRequestPortOpenOut,
        	stream<bool >&  		lbPortOpenReplyIn,
        	stream<axiWord> 	   &lb_packetBuffer,
        	stream<ap_uint<16> >   &lb_lengthBuffer,
        	stream<metadata>	   &lb_metadataBuffer) {
#pragma HLS PIPELINE II=1

	static enum sState {LB_IDLE = 0, LB_W8FORPORT, LB_ACC_FIRST, LB_ACC} sinkState;
	static uint16_t 			lbPacketLength 		= 0;
	//static ap_uint<32> 			openPortWaitTime 	= TIME_5S;
	static ap_uint<32> 			openPortWaitTime 	= 100;
	
	switch(sinkState) {
		case LB_IDLE:
			if(!lbRequestPortOpenOut.full() && openPortWaitTime == 0) {
				lbRequestPortOpenOut.write(PORT);
				sinkState = LB_W8FORPORT;
			}
			else 
				openPortWaitTime--;
			break;
		case LB_W8FORPORT:
			if(!lbPortOpenReplyIn.empty()) {
				bool openPort = lbPortOpenReplyIn.read();
				sinkState = LB_ACC_FIRST;
			}
			break;
		case LB_ACC_FIRST:
            //wait until all the data has arrived, and output fifos are not not empty
			if (!lbRxDataIn.empty() && !lb_packetBuffer.full() && !lbRxMetadataIn.empty() && !lb_metadataBuffer.full()) {
				metadata tempMetadata = lbRxMetadataIn.read();
				sockaddr_in tempSocket = tempMetadata.sourceSocket ;
				tempMetadata.sourceSocket = tempMetadata.destinationSocket;
				tempMetadata.destinationSocket = tempSocket;
				lb_metadataBuffer.write(tempMetadata);
				axiWord tempWord = lbRxDataIn.read();
				lb_packetBuffer.write(tempWord);
				ap_uint<4> counter = 0;
                //keep track of how many bytes are read
				for (uint8_t i=0;i<8;++i) {
					if (tempWord.keep.bit(i) == 1)
						counter++;
				}
				lbPacketLength += counter;
				if (tempWord.last) {
					lb_lengthBuffer.write(lbPacketLength);
					lbPacketLength = 0;
				}
				else
					sinkState = LB_ACC;
			}
			break;
		case LB_ACC:
			if (!lbRxDataIn.empty() && !lb_packetBuffer.full()) {
				axiWord tempWord = lbRxDataIn.read();
				lb_packetBuffer.write(tempWord);
				ap_uint<4> counter = 0;
				for (uint8_t i=0;i<8;++i) {
					if (tempWord.keep.bit(i) == 1)
						counter++;
				}

				lbPacketLength += counter;
				if (tempWord.last) {
					lb_lengthBuffer.write(lbPacketLength);
					lbPacketLength = 0;
					sinkState = LB_ACC_FIRST;
				}
			}
			break;
	}
}
void txPath(stream<axiWord> 	   &lb_packetBuffer,
    		stream<ap_uint<16> >   &lb_lengthBuffer,
    		stream<metadata>	   &lb_metadataBuffer,
    		stream<axiWord> 	   &lbTxDataOut,
		 	stream<metadata> 	   &lbTxMetadataOut,
		 	stream<ap_uint<16> >   &lbTxLengthOut) {
#pragma HLS PIPELINE II=1
	if (!lb_packetBuffer.empty() && !lbTxDataOut.full())
    {
        axiWord tempData = lb_packetBuffer.read();
        axiWord tempDataCap;
        for(uint8_t i = 0; i < 8; i++)
        {
            if(tempData.data((i+1)*8-1, i*8) >= 97 && tempData.data((i+1)*8-1, i*8) <= 123)
            {
                tempDataCap.data((i+1)*8-1, i*8) = tempData.data((i+1)*8-1, i*8) - 32;
            }
            else
            {
                tempDataCap.data((i+1)*8-1, i*8) = tempData.data((i+1)*8-1, i*8);
            }
        }
        tempDataCap.last = tempData.last; 
        tempDataCap.keep = tempData.keep;
		lbTxDataOut.write(tempDataCap);
    }
	if (!lb_metadataBuffer.empty()  && !lbTxMetadataOut.full())
		lbTxMetadataOut.write(lb_metadataBuffer.read());
	if (!lb_lengthBuffer.empty() && !lbTxLengthOut.full())
		lbTxLengthOut.write(lb_lengthBuffer.read());
}

void udpCapitalize(stream<axiWord>&       lbRxDataIn,
                 stream<metadata>&     	lbRxMetadataIn,
				 stream<ap_uint<16> >&  lbRequestPortOpenOut,
				 stream<bool >&			lbPortOpenReplyIn,
				 stream<axiWord> 		&lbTxDataOut,
				 stream<metadata> 		&lbTxMetadataOut,
				 stream<ap_uint<16> > 	&lbTxLengthOut,
                 ap_uint<16>            &lbRxPort) {
	#pragma HLS INTERFACE ap_ctrl_none port=return
	#pragma HLS DATAFLOW

    #pragma HLS INTERFACE ap_none            port=lbRxPort             bundle=lbRxPort
    //#pragma HLS INTERFACE axis  port=lbRxDataIn           bundle=lbRxDataIn
    //#pragma HLS INTERFACE axis  port=lbRxMetadataIn       bundle=lbRxMetadataIn
    //#pragma HLS INTERFACE axis  port=lbRequestPortOpenOut bundle=lbRequestPortOpenOut
    //#pragma HLS INTERFACE axis  port=lbPortOpenReplyIn    bundle=lbPortOpenReplyIn
    //#pragma HLS INTERFACE axis  port=lbTxDataOut          bundle=lbTxDataOut
    //#pragma HLS INTERFACE axis  port=lbTxMetadataOut      bundle=lbTxMetadataOut
    //#pragma HLS INTERFACE axis  port=lbTxLengthOut        bundle=lbTxLengthOut
    #pragma HLS resource core=AXI4Stream variable=lbRxDataIn           metadata="-bus_bundle lbRxDataIn"
    #pragma HLS resource core=AXI4Stream variable=lbRxMetadataIn       metadata="-bus_bundle lbRxMetadataIn"
    #pragma HLS resource core=AXI4Stream variable=lbRequestPortOpenOut metadata="-bus_bundle lbRequestPortOpenOut"
    #pragma HLS resource core=AXI4Stream variable=lbPortOpenReplyIn    metadata="-bus_bundle lbPortOpenReplyIn"
    #pragma HLS resource core=AXI4Stream variable=lbTxDataOut          metadata="-bus_bundle lbTxDataOut"
    #pragma HLS resource core=AXI4Stream variable=lbTxMetadataOut      metadata="-bus_bundle lbTxMetadataOut"
    #pragma HLS resource core=AXI4Stream variable=lbTxLengthOut        metadata="-bus_bundle lbTxLengthOut"
  	#pragma HLS DATA_PACK variable=lbRxMetadataIn
  	#pragma HLS DATA_PACK variable=lbTxMetadataOut

	static stream<axiWord> 		lb_packetBuffer("lb_packetBuffer");
	static stream<ap_uint<16> > lb_lengthBuffer("lb_lengthBuffer");
	static stream<metadata>		lb_metadataBuffer("lb_metadataBuffer");
	#pragma HLS DATA_PACK variable 	= lb_packetBuffer
	#pragma HLS STREAM variable 	= lb_packetBuffer	depth = 1024
    lbRxPort = PORT;

	rxPath(lbRxDataIn, lbRxMetadataIn, lbRequestPortOpenOut, lbPortOpenReplyIn, lb_packetBuffer, lb_lengthBuffer, lb_metadataBuffer);
	txPath(lb_packetBuffer, lb_lengthBuffer, lb_metadataBuffer, lbTxDataOut, lbTxMetadataOut, lbTxLengthOut);
}
