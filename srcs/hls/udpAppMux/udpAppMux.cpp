/************************************************
  Copyright (c) 2016, Xilinx, Inc.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, 
  this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, 
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors 
  may be used to endorse or promote products derived from this software 
  without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.// Copyright (c) 2015 Xilinx, Inc.
 ************************************************/
#include "udpAppMux.hpp"
//uint16_t ports[NUM_MERGE_STREAMS] = {0x0281, 0x0282}; 

void appMuxRxPath(stream <axiWord>    & rxDataIn,
        stream <metadata>  & rxMetadataIn,
        stream <axiWord>   & rxDataOutDhcp,
        stream <metadata>  & rxMetadataOutDhcp,

        stream <axiWord>  &rxDataOutApp0,
        stream <metadata> &rxMetadataOutApp0,
        ap_uint<16>       &rxPortApp0,

        stream <axiWord>  &rxDataOutApp1,
        stream <metadata> &rxMetadataOutApp1,
        ap_uint<16>       &rxPortApp1
        ) {
#pragma HLS PIPELINE II=1
    static enum sState {LB_IDLE = 0, LB_IDLE2, LB_FIRST, LB_ACC} shimState;
    static ap_uint< LOG2_NUM_MERGE_STREAMS+1 > streamSourceRx = 0;

    static metadata tempMetadata;
#pragma HLS DATA_PACK variable=tempMetadata
    switch(shimState) {
        case LB_IDLE:
            {
                if (!rxMetadataIn.empty()) {
                    tempMetadata = rxMetadataIn.read();
                    shimState = LB_IDLE2;
                }
            }
            break;
        case LB_IDLE2:
            {
                if (tempMetadata.destinationSocket.port == 0x0044 && !rxMetadataOutDhcp.full()) {
                    rxMetadataOutDhcp.write(tempMetadata);
                    streamSourceRx = 0;
                    shimState = LB_FIRST;
                }
                else if(tempMetadata.destinationSocket.port == rxPortApp0 && !rxMetadataOutApp0.full())
                {
                    rxMetadataOutApp0.write(tempMetadata);
                    streamSourceRx = 1;
                    shimState = LB_FIRST;
                }
                else if(tempMetadata.destinationSocket.port == rxPortApp1 && !rxMetadataOutApp1.full())
                {

                    rxMetadataOutApp1.write(tempMetadata);
                    streamSourceRx = 2;
                    shimState = LB_FIRST;
                }
            }
            break;
        case LB_FIRST:
            {
                if (!rxDataIn.empty()) {
                    if ( streamSourceRx == 0 && !rxDataOutDhcp.full()) {
                        axiWord outputWord      = rxDataIn.read();
                        rxDataOutDhcp.write(outputWord);
                        if (!outputWord.last)
                            shimState = LB_ACC;
                        else if (outputWord.last)
                            shimState = LB_IDLE;
                    }
                    else if(streamSourceRx == 1 && !rxDataOutApp0.full())
                    {
                        axiWord outputWord      = rxDataIn.read();
                        rxDataOutApp0.write(outputWord);
                        if (!outputWord.last)
                            shimState = LB_ACC;
                        else if (outputWord.last)
                            shimState = LB_IDLE;
                    }
                    else if(streamSourceRx == 2 && !rxDataOutApp1.full())
                    {
                        axiWord outputWord      = rxDataIn.read();
                        rxDataOutApp1.write(outputWord);
                        if (!outputWord.last)
                            shimState = LB_ACC;
                        else if (outputWord.last)
                            shimState = LB_IDLE;
                    }
                }
            }
            break;
        case LB_ACC:
            {
                if (!rxDataIn.empty()) {
                    if (streamSourceRx == 0 && !rxDataOutDhcp.full()) {
                        axiWord outputWord = rxDataIn.read();
                        rxDataOutDhcp.write(outputWord);
                        if (outputWord.last)
                            shimState = LB_IDLE;
                    }
                    else if (streamSourceRx == 1 && !rxDataOutApp0.full()) {
                        axiWord outputWord = rxDataIn.read();
                        rxDataOutApp0.write(outputWord);
                        if (outputWord.last)
                            shimState = LB_IDLE;
                    }
                    else if (streamSourceRx == 2 && !rxDataOutApp1.full()) {
                        axiWord outputWord = rxDataIn.read();
                        rxDataOutApp1.write(outputWord);
                        if (outputWord.last)
                            shimState = LB_IDLE;
                    }

                }
            }
            break;
    }
}
//Merges data from multiple UDP streams
void appMuxTxPath(stream<axiWord>        & txDataInDhcp,
        stream <metadata>      & txMetadataInDhcp,
        stream <ap_uint<16> >  & txLengthInDhcp,

        stream <axiWord>       &txDataInApp0,
        stream <metadata>      &txMetadataInApp0,
        stream <ap_uint<16> >  &txLengthInApp0,

        stream <axiWord>       &txDataInApp1,
        stream <metadata>      &txMetadataInApp1,
        stream <ap_uint<16> >  &txLengthInApp1,

        stream <axiWord>       & txDataOut,
        stream <metadata>      & txMetadataOut,
        stream <ap_uint<16> >  & txLengthOut) {
#pragma HLS PIPELINE II=1
    static enum txsState {SHIM_IDLE = 0, SHIM_STREAM} shimState_tx;
    static ap_uint< LOG2_NUM_MERGE_STREAMS+1 >	streamSource = 0;
    static ap_uint< LOG2_NUM_MERGE_STREAMS+1 > rr_counter = 0;
    switch(shimState_tx) {
        case SHIM_IDLE:
            if (!txDataOut.full() && !txMetadataOut.full() && !txLengthOut.full()) {
                if(rr_counter == 0)
                {
                    if (!txDataInDhcp.empty() && !txMetadataInDhcp.empty() && !txLengthInDhcp.empty()) {
                        streamSource = 0;
                        axiWord outputWord = txDataInDhcp.read();
                        txDataOut.write(outputWord);
                        txMetadataOut.write(txMetadataInDhcp.read());
                        txLengthOut.write(txLengthInDhcp.read());
                        if (outputWord.last == 0)
                            shimState_tx = SHIM_STREAM;
                    }
                    ++rr_counter;
                }
                else if(rr_counter == 1)
                {
                    if (!txDataInApp0.empty() && !txMetadataInApp0.empty() && !txLengthInApp0.empty()) 
                    {
                        streamSource = 1;
                        axiWord outputWord = txDataInApp0.read();
                        txDataOut.write(outputWord);
                        metadata outputMetadata = txMetadataInApp0.read();

                        txMetadataOut.write(outputMetadata);
                        txLengthOut.write(txLengthInApp0.read());
                        if (outputWord.last == 0)
                            shimState_tx = SHIM_STREAM;
                    }
                    ++rr_counter;
                }
                else if (rr_counter == 2)
                {
                    if (!txDataInApp1.empty() && !txMetadataInApp1.empty() && !txLengthInApp1.empty()) 
                    {
                        streamSource = 2;
                        axiWord outputWord = txDataInApp1.read();
                        txDataOut.write(outputWord);
                        metadata outputMetadata = txMetadataInApp1.read();

                        txMetadataOut.write(outputMetadata);
                        txLengthOut.write(txLengthInApp1.read());
                        if (outputWord.last == 0)
                            shimState_tx = SHIM_STREAM;
                    }
                    rr_counter = 0;
                }
			}
			break;
		case SHIM_STREAM:
			if (!txDataOut.full()) {
				if (streamSource == 0 && !txDataInDhcp.empty()) {
					axiWord outputWord = txDataInDhcp.read();
					txDataOut.write(outputWord);
					if (outputWord.last == 1)
						shimState_tx = SHIM_IDLE;
				}
				else if (streamSource == 1 && !txDataInApp0.empty()) {
					axiWord outputWord = txDataInApp0.read();
					txDataOut.write(outputWord);
					if (outputWord.last == 1)
						shimState_tx = SHIM_IDLE;
				}
				else if (streamSource == 2 && !txDataInApp1.empty()) {
					axiWord outputWord = txDataInApp1.read();
					txDataOut.write(outputWord);
					if (outputWord.last == 1)
						shimState_tx = SHIM_IDLE;
				}
			}
			break;
	}
}

void appMuxPortPath(stream<ap_uint<16> > & requestPortOpenOut,
                  stream <bool >         & portOpenReplyIn,

                  stream <ap_uint<16> >  & requestPortOpenInDhcp,
                  stream <bool >         & portOpenReplyOutDhcp,

                  stream <ap_uint<16> >  &requestPortOpenInApp0,
                  stream <bool >         &portOpenReplyOutApp0,
                  stream <ap_uint<16> >  &requestPortOpenInApp1,
                  stream <bool >         &portOpenReplyOutApp1
                  
                  ) {
#pragma HLS PIPELINE II=1
    static enum portState {PORT_IDLE = 0, PORT_STREAM} shimStatePort;
    static ap_uint< LOG2_NUM_MERGE_STREAMS+1 >   streamSourcePort = 0;
    
    switch(shimStatePort) {
        case PORT_IDLE:
            if (!requestPortOpenOut.full()) {
                if (!requestPortOpenInDhcp.empty()) {
                    requestPortOpenOut.write(requestPortOpenInDhcp.read());
                    streamSourcePort = 0;
                    shimStatePort = PORT_STREAM;
                }
                else if (!requestPortOpenInApp0.empty()) 
                {
                    requestPortOpenOut.write(requestPortOpenInApp0.read());
                    streamSourcePort = 1;
                    shimStatePort = PORT_STREAM;
                    break;
                }
                else if (!requestPortOpenInApp1.empty()) 
                {
                    requestPortOpenOut.write(requestPortOpenInApp1.read());
                    streamSourcePort = 2;
                    shimStatePort = PORT_STREAM;
                    break;
                }
            }
            break;
        case PORT_STREAM:
            if (!portOpenReplyIn.empty()) {
                if (streamSourcePort == 0 && !portOpenReplyOutDhcp.full()) 
                {
                    portOpenReplyOutDhcp.write(portOpenReplyIn.read());
                    shimStatePort = PORT_IDLE;
                }
                else if (streamSourcePort == 1 && !portOpenReplyOutApp0.full()) 
                {
                    portOpenReplyOutApp0.write(portOpenReplyIn.read());
                    shimStatePort = PORT_IDLE;
                    break;
                }
                else if (streamSourcePort == 2 && !portOpenReplyOutApp1.full()) 
                {
                    portOpenReplyOutApp1.write(portOpenReplyIn.read());
                    shimStatePort = PORT_IDLE;
                    break;
                }
            }
            break;
    }
}
             
void udpAppMux( stream <axiWord>      &rxDataIn,
                stream <metadata>     &rxMetadataIn,

                stream <axiWord>      &rxDataOutDhcp,
                stream <metadata>     &rxMetadataOutDhcp,

                stream <axiWord>      &rxDataOutApp0,
                stream <metadata>     &rxMetadataOutApp0,
                ap_uint<16>           &rxPortApp0,
                stream <axiWord>      &rxDataOutApp1,
                stream <metadata>     &rxMetadataOutApp1,
                ap_uint<16>           &rxPortApp1,


                stream <ap_uint<16> > &requestPortOpenOut,
                stream <bool >        &portOpenReplyIn,

                stream <ap_uint<16> > &requestPortOpenInDhcp,
                stream <bool >        &portOpenReplyOutDhcp,

                stream <ap_uint<16> > &requestPortOpenInApp0,
                stream <bool >        &portOpenReplyOutApp0,
                stream <ap_uint<16> > &requestPortOpenInApp1,
                stream <bool >        &portOpenReplyOutApp1,


                stream <axiWord>      & txDataInDhcp,
                stream <metadata>     & txMetadataInDhcp,
                stream <ap_uint<16> > & txLengthInDhcp,

                stream <axiWord>      &txDataInApp0,
                stream <metadata>     &txMetadataInApp0,
                stream <ap_uint<16> > &txLengthInApp0,
                stream <axiWord>      &txDataInApp1,
                stream <metadata>     &txMetadataInApp1,
                stream <ap_uint<16> > &txLengthInApp1,

                stream <axiWord>      & txDataOut,
                stream <metadata>     & txMetadataOut,
                stream <ap_uint<16> > & txLengthOut) {
    #pragma HLS INTERFACE ap_ctrl_none port=return
    #pragma HLS DATAFLOW interval=1
    
    #pragma HLS INTERFACE ap_stable port=rxPortApp1
    #pragma HLS INTERFACE ap_stable port=rxPortApp0

    #pragma HLS resource core=AXI4Stream variable=rxDataIn              metadata="-bus_bundle rxDataIn"
    #pragma HLS resource core=AXI4Stream variable=rxMetadataIn          metadata="-bus_bundle rxMetadataIn"
    #pragma HLS resource core=AXI4Stream variable=rxDataOutDhcp         metadata="-bus_bundle rxDataOutDhcp"
    #pragma HLS resource core=AXI4Stream variable=rxMetadataOutDhcp     metadata="-bus_bundle rxMetadataOutDhcp"
    #pragma HLS resource core=AXI4Stream variable=rxDataOutApp0         metadata="-bus_bundle rxDataOutApp0"
    #pragma HLS resource core=AXI4Stream variable=rxDataOutApp1         metadata="-bus_bundle rxDataOutApp1"
    #pragma HLS resource core=AXI4Stream variable=rxMetadataOutApp0     metadata="-bus_bundle rxMetadataOutApp0"
    #pragma HLS resource core=AXI4Stream variable=rxMetadataOutApp1     metadata="-bus_bundle rxMetadataOutApp1"
    #pragma HLS resource core=AXI4Stream variable=requestPortOpenOut    metadata="-bus_bundle requestPortOpenOut"
    #pragma HLS resource core=AXI4Stream variable=portOpenReplyIn       metadata="-bus_bundle portOpenReplyIn"
    #pragma HLS resource core=AXI4Stream variable=requestPortOpenInDhcp metadata="-bus_bundle requestPortOpenInDhcp"
    #pragma HLS resource core=AXI4Stream variable=portOpenReplyOutDhcp  metadata="-bus_bundle portOpenReplyOutDhcp"
    #pragma HLS resource core=AXI4Stream variable=requestPortOpenInApp0 metadata="-bus_bundle requestPortOpenInApp0"
    #pragma HLS resource core=AXI4Stream variable=portOpenReplyOutApp0  metadata="-bus_bundle portOpenReplyOutApp0"
    #pragma HLS resource core=AXI4Stream variable=requestPortOpenInApp1 metadata="-bus_bundle requestPortOpenInApp1"
    #pragma HLS resource core=AXI4Stream variable=portOpenReplyOutApp1  metadata="-bus_bundle portOpenReplyOutApp1"
    #pragma HLS resource core=AXI4Stream variable=txDataInDhcp          metadata="-bus_bundle txDataInDhcp"
    #pragma HLS resource core=AXI4Stream variable=txMetadataInDhcp      metadata="-bus_bundle txMetadataInDhcp"
    #pragma HLS resource core=AXI4Stream variable=txLengthInDhcp        metadata="-bus_bundle txLengthInDhcp"
    #pragma HLS resource core=AXI4Stream variable=txDataInApp0          metadata="-bus_bundle txDataInApp0"
    #pragma HLS resource core=AXI4Stream variable=txMetadataInApp0      metadata="-bus_bundle txMetadataInApp0"
    #pragma HLS resource core=AXI4Stream variable=txLengthInApp0        metadata="-bus_bundle txLengthInApp0"
    #pragma HLS resource core=AXI4Stream variable=txDataInApp1          metadata="-bus_bundle txDataInApp1"
    #pragma HLS resource core=AXI4Stream variable=txMetadataInApp1      metadata="-bus_bundle txMetadataInApp1"
    #pragma HLS resource core=AXI4Stream variable=txLengthInApp1        metadata="-bus_bundle txLengthInApp1"
    #pragma HLS resource core=AXI4Stream variable=txDataOut             metadata="-bus_bundle txDataOut"
    #pragma HLS resource core=AXI4Stream variable=txMetadataOut         metadata="-bus_bundle txMetadataOut"
    #pragma HLS resource core=AXI4Stream variable=txLengthOut           metadata="-bus_bundle txLengthOut"
    //#pragma HLS INTERFACE axis port=rxDataIn
    //#pragma HLS INTERFACE axis port=rxMetadataIn
    //#pragma HLS INTERFACE axis port=rxDataOutDhcp
    //#pragma HLS INTERFACE axis port=rxMetadataOutDhcp
    //#pragma HLS INTERFACE axis port=rxDataOutApp0
    //#pragma HLS INTERFACE axis port=rxDataOutApp1
    //#pragma HLS INTERFACE axis port=rxMetadataOutApp0
    //#pragma HLS INTERFACE axis port=rxMetadataOutApp1

    //#pragma HLS INTERFACE axis port=requestPortOpenOut
    //#pragma HLS INTERFACE axis port=portOpenReplyIn
    //#pragma HLS INTERFACE axis port=requestPortOpenInDhcp
    //#pragma HLS INTERFACE axis port=portOpenReplyOutDhcp
    //#pragma HLS INTERFACE axis port=requestPortOpenInApp0
    //#pragma HLS INTERFACE axis port=portOpenReplyOutApp0
    //#pragma HLS INTERFACE axis port=requestPortOpenInApp1
    //#pragma HLS INTERFACE axis port=portOpenReplyOutApp1

    //#pragma HLS INTERFACE axis port=txDataInDhcp
    //#pragma HLS INTERFACE axis port=txMetadataInDhcp
    //#pragma HLS INTERFACE axis port=txLengthInDhcp
    //#pragma HLS INTERFACE axis port=txDataInApp0
    //#pragma HLS INTERFACE axis port=txMetadataInApp0
    //#pragma HLS INTERFACE axis port=txLengthInApp0
    //#pragma HLS INTERFACE axis port=txDataInApp1
    //#pragma HLS INTERFACE axis port=txMetadataInApp1
    //#pragma HLS INTERFACE axis port=txLengthInApp1
    //#pragma HLS INTERFACE axis port=txDataOut
    //#pragma HLS INTERFACE axis port=txMetadataOut
    //#pragma HLS INTERFACE axis port=txLengthOut

    //#pragma HLS DATA_PACK variable=rxDataIn
    #pragma HLS DATA_PACK variable=rxMetadataIn
    //#pragma HLS DATA_PACK variable=rxDataOutDhcp
    #pragma HLS DATA_PACK variable=rxMetadataOutDhcp
    //#pragma HLS DATA_PACK variable=rxDataOutApp
    #pragma HLS DATA_PACK variable=rxMetadataOutApp0
    #pragma HLS DATA_PACK variable=rxMetadataOutApp1


    #pragma HLS DATA_PACK variable=txMetadataInDhcp
    #pragma HLS DATA_PACK variable=txMetadataInApp0
    #pragma HLS DATA_PACK variable=txMetadataInApp1
    #pragma HLS DATA_PACK variable=txMetadataOut

    appMuxRxPath(rxDataIn, rxMetadataIn, 
                 rxDataOutDhcp, rxMetadataOutDhcp,
                 rxDataOutApp0, rxMetadataOutApp0, rxPortApp0,
                 rxDataOutApp1, rxMetadataOutApp1, rxPortApp1);

    appMuxPortPath(requestPortOpenOut, portOpenReplyIn,
                   requestPortOpenInDhcp, portOpenReplyOutDhcp,
                   requestPortOpenInApp0, portOpenReplyOutApp0,
                   requestPortOpenInApp1, portOpenReplyOutApp1);

    appMuxTxPath(txDataInDhcp, txMetadataInDhcp, txLengthInDhcp,
                 txDataInApp0, txMetadataInApp0, txLengthInApp0, 
                 txDataInApp1, txMetadataInApp1, txLengthInApp1, 
                 txDataOut, txMetadataOut, txLengthOut);
}
